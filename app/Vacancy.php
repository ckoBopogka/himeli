<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Vacancy extends Model
{
    const IS_DRAFT = 0;
    const IS_PUBLIC = 1;

    protected $fillable = [
        'title',
        'date',
        'content'
    ];

    /**
     * @param $fields
     * @return vacancy
     */
    public static function add($fields)
    {
        $vacancy = new static;
        $vacancy->fill($fields);
        $vacancy->save();

        return $vacancy;
    }

    /**
     * @param $fields
     */
    public function edit($fields): void
    {
        $this->fill($fields);
        $this->save();
    }

    /**
     * @throws \Exception
     */
    public function remove(): void
    {
        $this->delete();
    }

    /**
     * Set status draft
     */
    public function setDraft()
    {
        $this->status = Vacancy::IS_DRAFT;
        $this->save();
    }

    /**
     * Set status public
     */
    public function setPublic()
    {
        $this->status = Vacancy::IS_PUBLIC;
        $this->save();
    }

    /**
     * @param $value
     */
    public function toggleStatus($value)
    {
        if ($value == null) {
            return $this->setDraft();
        }

        return $this->setPublic();
    }

    public function setUnHot()
    {
        $this->is_hot = Vacancy::IS_DRAFT;
        $this->save();
    }

    /**
     * Set status hot
     */
    public function setHot()
    {
        $this->is_hot = Vacancy::IS_PUBLIC;
        $this->save();
    }

    /**
     * @param $value
     */
    public function toggleHots($value)
    {
        if ($value == null) {
            return $this->setUnHot();
        }

        return $this->setHot();
    }

    public function the_content(): void
    {
        echo $this->content;
    }

    public function setDateAttribute($value)
    {
        $date = Carbon::createFromFormat('d.m.y', $value)->format('Y-m-d');
        $this->attributes['date'] = $date;
    }

    public function getDateAttribute($value)
    {
        $date = Carbon::createFromFormat('Y-m-d', $value)->format('d.m.y');
        return $date;
    }
}
