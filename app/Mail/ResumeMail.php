<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ResumeMail extends Mailable
{
    use Queueable, SerializesModels;

    public $resume;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct( $resume )
    {
        $this->resume = $resume;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('careers@himeli.org')
            ->subject('New resume from site')
            ->text('emails.resume')
            ->with(
                [
                    'resume' => $this->resume,
                ])
            ->attach($this->resume->resume->getRealPath(),
                [
//                    'mime' => 'application/octet-stream',
                    'as' => $this->resume->resume->getClientOriginalName(),
                    'mime' => $this->resume->resume->getClientMimeType()
                ]
            );
    }
}
