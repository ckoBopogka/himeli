<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class User extends Authenticatable
{
    use Notifiable;

    const IS_BAN = 1;
    const IS_ACTIVE = 0;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'avatar'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    /**
     * @param $fields
     * @return User
     */
    public static function add($fields)
    {
        $user = new static;
        $user->fill($fields);
        $user->makeAdmin();
        $user->save();

        return $user;
    }

    /**
     * @param $fields
     */
    public function edit($fields)
    {
        $this->fill($fields);
        $this->generatePassword($fields['password']);
        $this->save();
    }

    public function generatePassword($password)
    {
        if ($password != null) {
            $this->password = bcrypt($password);
            $this->save();
        }
    }

    /**
     * @throws \Exception
     */
    public function remove()
    {
        $this->removeAvatar();
        $this->delete();
    }

    /**
     * @param $image
     */
    public function uploadAvatar($image) : void
    {
        if($image == null) { return; }

        $this->removeAvatar();

        $filename = str_random(10) . '.' . $image->extension();
        $image->storeAs('uploads', $filename);
        $this->avatar = $filename;
        $this->save();
    }

    public function removeAvatar() : void
    {
        if ($this->avatar != null) {
            Storage::delete('uploads/' . $this->avatar);
        }
    }


    /**
     * @return string
     */
    public function getAvatar() : string
    {
        if ($this->avatar == null) {
            return '/img/no-user-image.png';
        }

        return '/uploads/' . $this->avatar;
    }

    /**
     * Set status admin = 1
     */
    public function makeAdmin()
    {
        $this->is_admin = 1;
        $this->save();
    }

    /**
     * Set status admin = 0
     */
    public function makeNormal()
    {
        $this->is_admin = 0;
        $this->save();
    }

    /**
     * @param $value
     */
    public function toggleAdmin($value)
    {
        if ($value == null){
            return $this->makeNormal();
        }

        return $this->makeAdmin();
    }

    /**
     * Set banned user = 1
     */
    public function ban()
    {
        $this->status = User::IS_BANNED;
        $this->save();
    }

    /**
     * Set banned user = 0
     */
    public function unban()
    {
        $this->status = User::IS_ACTIVE;
        $this->save();
    }

    /**
     * @param $value
     */
    public function toggleBan($value)
    {
        if ( $value == null ) {
            return $this->unban();
        }

        return $this->ban();
    }
}
