<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class Post extends Model
{

    use Sluggable;

    const IS_DRAFT = 0;
    const IS_PUBLIC = 1;

    protected $fillable = [
        'title',
        'date',
        'content',
        'excerpt',
        'views',
		'slug',
		'keywords',
        'time'
    ];

    /**
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags()
    {
        return $this->belongsToMany(
            Tag::class,
            'post_tags',
            'post_id',
            'tag_id'
        );
    }

    /**
     * @param $fields
     * @return Post
     */
    public static function add($fields)
    {
        $post = new static;
        $post->fill($fields);
        $post->user_id = 1;
        $post->save();

        return $post;
    }

    /**
     * @param $fields
     */
    public function edit($fields)
    {
        $this->fill($fields);
        $this->save();
    }

    /**
     * @throws \Exception
     */
    public function remove()
    {
        $this->removeImage();
        $this->delete();
    }

    /**
     * @param $image
     */
    public function uploadImage( $image )
    {
        if ( $image == null ) {
            return;
        }

        $this->removeImage();
        $filename = Str::random(10) . '.' . $image->extension();
        $image->storeAs('uploads', $filename);
        $this->image = $filename;
        $this->save();
    }

    /**
     * @return string
     */
    public function getImage()
    {
        if ( $this->image == null ) {
            return '/img/no-image.png';
        }

        return '/uploads/' . $this->image;
    }

    public function removeImage()
    {
        if ($this->image != null) {
            Storage::delete('uploads/' . $this->image);
        }
    }

    /**
     * @param $id
     */
    public function setCategory($id)
    {
        if ( $id == null ) {
            return;
        }

        $this->category_id = $id;
        $this->save();
    }

    /**
     * @param $ids
     */
    public function setTags($ids)
    {
        if ( $ids == null ) {
            return;
        }

        $this->tags()->sync($ids);
    }

    /**
     * Set status draft
     */
    public function setDraft()
    {
        $this->status = Post::IS_DRAFT;
        $this->save();
    }

    /**
     * Set status public
     */
    public function setPublic()
    {
        $this->status = Post::IS_PUBLIC;
        $this->save();
    }

    /**
     * @param $value
     */
    public function toggleStatus($value)
    {
        if ($value == null) {
            return $this->setDraft();
        }

        return $this->setPublic();
    }

    /**
     * Set Featured Post
     */
    public function setFeatured()
    {
        $this->is_featured = 1;
        $this->save();
    }

    /**
     * Set Standard Post
     */
    public function setStandart()
    {
        $this->is_featured = 0;
        $this->save();
    }

    public function toggleFeatured($value)
    {
        if ($value == null) {
            return $this->setStandart();
        }

        return $this->setFeatured();
    }

    public function setCounterView()
    {
        $count = $this->views;
        $this->views = (int)$count + 1;
        $this->save();
    }

    public function setDateAttribute($value)
    {
        $date = Carbon::createFromFormat('d.m.y', $value)->format('Y-m-d');
        $this->attributes['date'] = $date;
    }

    public function getDateAttribute($value)
    {
        $date = Carbon::createFromFormat('Y-m-d', $value)->format('d.m.y');
        return $date;
    }

    public function getTagsTitles()
    {
        return ( !$this->tags->isEmpty() ) ? implode(', ', $this->tags->pluck('title')->all()) : 'Нет тегов';
    }

    public function getTagListPost() : array
    {
        $list = [];

        if ( ! $this->tags->isEmpty() ) {
            $list = $this->tags->pluck('title', 'slug')->all();
        }

        return $list;
    }

    public function getCategoryID()
    {
        return $this->category != null ? $this->category->id : null;
    }

    public function getCategoryTitle()
    {
        return ($this->category != null) ? $this->category->title : 'Нет категории';
    }

    public function getCategoryUrl(): string
    {
        return ($this->category != null) ? route('home') . '/category/' . $this->category->slug : '/';
    }

    public function hasPrevious()
    {
        return self::where([
            ['category_id', '=', $this->getCategoryID()],
            ['status', '=', '0'],
            ['id', '<', $this->id]
        ])->max('id');
    }

    public function getPrevious()
    {
        $postID = $this->hasPrevious();
        return self::find($postID);
    }

    public function hasNext()
    {
        return self::where([
            ['category_id', '=', $this->getCategoryID()],
            ['status', '=', '0'],
            ['id', '>', $this->id]
        ])->min('id');
    }

    public function getNext()
    {
        $postID = $this->hasNext();
        return self::find($postID);
    }

    public function getTargetPost()
    {
        if ( $this->getNext() !== null) {
            return $this->getNext();
        } else {
            return $this->getPrevious();
        }
    }

    public static function getPopularPosts()
    {
        return self::orderBy('views','desc')->take(3)->get();
    }

    public function related()
    {
        return self::all()->except($this->id);
    }

    public function the_content() : void
    {
        echo $this->content;
    }

    public function the_excerpt() : void
    {
        echo $this->excerpt;
    }

    public function the_slug() : string
    {
        return '/news/' . $this->slug;
    }
}
