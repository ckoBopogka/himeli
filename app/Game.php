<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class Game extends Model
{
    use Sluggable;

    protected $fillable = [
        'title',
        'content',
        'except',
        'date',
        'play_market_link',
        'app_store_link',
        'forum_link',
		'slug',
		'keywords'
    ];

    /**
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }

    /**
     * @param $fields
     * @return Game
     */
    public static function add($fields)
    {
        $game = new static();
        $game->fill($fields);
        $game->user__game_id = 1;
        $game->save();

        return $game;
    }

    /**
     * @param $fields
     */
    public function edit($fields)
    {
        $this->fill($fields);
        $this->save();
    }

    /**
     * @throws \Exception
     */
    public function remove()
    {
        $this->removeImage();
        $this->removeBanner();
        $this->delete();
    }

    /**
     * @param $image
     */
    public function uploadImage($thumbnail_game)
    {
        if ($thumbnail_game == null) { return; }

        $this->removeImage();
        $filename = Str::random(10) . '.' . $thumbnail_game->extension();
        $thumbnail_game->storeAs('uploads', $filename);
        $this->thumbnail_game = $filename;
        $this->save();
    }

    /**
     * @return string
     */
    public function getImage()
    {
        if ($this->thumbnail_game == null)
            return '/img/no-image.png'; // Заменить на другое изображение

        return '/uploads/' . $this->thumbnail_game;
    }

    /**
     * Remove image
     */
    public function removeImage()
    {
        if ($this->thumbnail_game != null) {
            Storage::delete('uploads/' . $this->thumbnail_game);
        }
    }

    /**
     * Upload Main Banner Game
     * @param $image
     */
    public function uploadBanner($banner_game)
    {
        if ($banner_game == null) { return; }

        $this->removeBanner();
        $filename = Str::random(10) . '.' . $banner_game->extension();
        $banner_game->storeAs('uploads', $filename);
        $this->banner_game = $filename;
        $this->save();
    }

    /**
     * @return string
     */
    public function getBanner()
    {
        if ($this->banner_game == null)
            return '/img/no-image.png'; // Заменить на другое изображение

        return '/uploads/' . $this->banner_game;
    }

    /**
     * Remove image
     */
    public function removeBanner()
    {
        if ($this->banner_game != null) {
            Storage::delete('uploads/' . $this->banner_game);
        }
    }

    /**
     * @param $files
     */
    public function uploadGallery($files)
    {
        if ($files == null) { return; }

        $this->removeGallery();

        $name_files = array();

        if( is_array($files) ) {
            foreach ($files as $key => $file) {
                $filename = Str::random(10) . '.' . $file->extension();
                $file->storeAs('uploads', $filename);
                $name_files[$key] = $filename;
            }
            $this->files_gallery = serialize($name_files);
            $this->save();
        }
    }

    public function getCountImgGallery() : int
    {
        if ($this->files_gallery == null) {
            return 0;
        }

        $gallery_imgs = unserialize($this->files_gallery);
        return count($gallery_imgs);
    }

    public function getImgGallery() : array
    {
        if ( $this->getCountImgGallery() == null ) {
            return $this->files_gallery = array();
        }

        $gallery_imgs = unserialize($this->files_gallery);
        return $gallery_imgs;
    }

    /**
     *
     */
    public function removeGallery()
    {
        if ($this->files_gallery == null) { return; }

        $gallery_imgs = unserialize($this->files_gallery);

        if (is_array($gallery_imgs)) {
            foreach ($gallery_imgs as $gallery_img) {
                Storage::delete('uploads/' . $gallery_img);
            }
        }
    }

    /**
     * Set status field = 0
     */
    public function setDraft()
    {
        $this->status = 0;
        $this->save();
    }

    /**
     * Set status field = 1
     */
    public function setPublic()
    {
        $this->status = 1;
        $this->save();
    }

    /**
     *
     * @param $value (status) = true or false
     */
    public function toggleStatus($value)
    {
        if ($value == null)
            return $this->setDraft();

        return $this->setPublic();
    }

    /**
     * Set is_feature field = 1
     */
    public function setFeatured()
    {
        $this->is_featured = 1;
        $this->save();
    }

    /**
     * Set is_feature field = 0
     */
    public function setStandart()
    {
        $this->is_featured = 0;
        $this->save();
    }

    /**
     * @param $value (is_feature) = true or false
     */
    public function toggleFeatured($value)
    {
        if ($value == null)
            return $this->setStandart();

        return $this->setFeatured();
    }

    /**
     * Set is_top = 1
     */
    public function setTop()
    {
        $this->is_top = 1;
        $this->save();
    }

    /**
     * Set is_top = 0
     */
    public function setUnTop()
    {
        $this->is_top = 0;
        $this->save();
    }

    /**
     * @param $value (is_top) = true or false
     */
    public function toggleTop($value)
    {
        if ($value === null) {
            return $this->setUnTop();
        }

        return $this->setTop();
    }

    /**
     * Set is_coming_soon = 1
     */
    public function comingSoon()
    {
        $this->is_coming_soon = 1;
        $this->save();
    }

    /**
     * Set is_coming_soon = 0
     */
    public function uncomingSoon()
    {
        $this->is_coming_soon = 0;
        $this->save();
    }

    /**
     * @param $value (is_coming_soon) = true or false
     */
    public function toggleComingSoon($value)
    {
        if ($value == null)
            return $this->uncomingSoon();

        return $this->comingSoon();
    }

    /**
     * Set is_popular = 1
     */
    public function popular()
    {
        $this->is_popular = 1;
        $this->save();
    }

    /**
     * Set is_popular = 0
     */
    public function unpopular()
    {
        $this->is_popular = 0;
        $this->save();
    }

    /**
     * @param $value(is_popular) = true or false
     */
    public function togglePopular($value)
    {
        if ($value == null)
            return $this->unpopular();

        return $this->popular();
    }

    /**
     * @param $value(date)
     */
    public function setDateAttribute($value)
    {
        $date = Carbon::createFromFormat('d.m.y', $value)->format('Y-m-d');
        $this->attributes['date'] = $date;
    }

    /**
     * @param $value(date)
     * @return string
     */
    public function getDateAttribute($value)
    {
        $date = Carbon::createFromFormat('Y-m-d', $value)->format('d.m.y');
        return $date;
    }

    public function the_content() : void
	{
		echo $this->content;
	}

}
