<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Setting extends Model
{
    private $image;

    protected $primaryKey = 'options_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'options_name',
        'options_value'
    ];

    public static function add( array $options_all )
    {
        $settings = new static;;

        foreach ( $options_all['options_name'] as $option_name => $option_value )
        {
            $settings = new Setting;
            $settings::updateOrCreate(
                ['options_name' => $option_name],
                ['options_value' => trim($option_value)]
            );
        }

        return $settings;
    }

    public function uploadIconSite( $image ): void
    {
        if ( $image === null ) {
            return;
        }

        $this->removeIconSite();

        $filename = Str::random(10) . '.' . $image->extension();
        $image->storeAs('uploads', $filename);
        $this->image = $filename;
        $this->save();
    }

    /**
     * @return string
     */
    public function getIconSite():string
    {
        if ( $this->image == null ) {
            return '/img/no-image.png';
        }

        return '/uploads/' . $this->image;
    }

}
