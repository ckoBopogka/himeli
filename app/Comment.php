<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function post()
    {
        return $this->hasOne(Post::class);
    }

    /**
     * Get Author comment
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function author()
    {
        return $this->hasOne(User::class);
    }

    /**
     * Set status comment = 1
     */
    public function allow()
    {
        $this->status = 1;
        $this->save();
    }

    /**
     * Set status comment = 0
     */
    public function disAllow()
    {
        $this->status = 0;
        $this->save();
    }

    /**
     * Set toggle status comment
     */
    public function toggleStatus()
    {
        if ($this->status = 0) {
            return $this->allow();
        }

        return $this->disAllow();
    }

    /**
     * Remove comment
     * @throws \Exception
     */
    public function remove()
    {
        $this->delete();
    }
}
