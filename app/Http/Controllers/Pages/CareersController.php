<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use App\Vacancy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Spatie\SchemaOrg\Schema;
use MetaTag;
use App\Mail\ResumeMail;

class CareersController extends Controller
{
    public function render_page(): object
    {
        $vacancies = Vacancy::orderBy('date', 'DESC')
            ->where('status', 0)
            ->get();

        MetaTag::set('title', 'Careers | HiMeli');
        MetaTag::set('description', 'Except for just developing games, we also provide game art services');
        MetaTag::set('keywords',
            'HiMeli Careers, Careers, gamedev, job, it, technology job');
        MetaTag::set('image', asset('img/office-banner.jpg'));

        $organization = Schema::organization()
            ->description('Except for just developing games, we also provide game art services')
            ->name('HiMeli')
            ->url( url()->full() )
            ->logo(env('APP_URL') . 'img/' . 'logoB_HiMeli_2020-10.svg')
            ->foundingDate('2016')
            ->sameAs( [
                "https://www.instagram.com/himeliofficial/",
                "https://www.facebook.com/himeliofficial/",
                "https://www.youtube.com/channel/UCRnPEtgvpJqacaaWmvb6WpA",
                "https://twitter.com/himeliofficial"
            ] );

        $person = Schema::person()
            ->name('HiMeli')
            ->url( env('APP_URL') )
            ->sameAs( [
                "https://www.instagram.com/himeliofficial/",
                "https://www.facebook.com/himeliofficial/",
                "https://www.youtube.com/channel/UCRnPEtgvpJqacaaWmvb6WpA",
                "https://twitter.com/himeliofficial"
            ] );

        return view('pages.careers' , [
            'vacancies'    => $vacancies,
            'organization' => $organization,
            'person'       => $person
        ] );
    }

    public function send_resume( Request $request ) {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'resume' => 'required|mimes:pages,docx,doc,odt,pdf|max:10000'
        ]);

        $resume_email = new \stdClass();
        $resume_email->email = $request->get('email');
        $resume_email->name = $request->get('name');
        $resume_email->phone = $request->get('phone');
        $resume_email->message = $request->get('message');
        $resume_email->resume = $request->file('resume');

        Mail::to("careers@himeli.org")->send( new ResumeMail( $resume_email ) );

        return redirect()->back()->with('status', 'Your resume send!');
    }
}
