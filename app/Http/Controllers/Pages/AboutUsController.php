<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use MetaTag;
use Spatie\SchemaOrg\Schema;

class AboutUsController extends Controller
{
    public function index(): object
    {
        MetaTag::set('title', 'About Us | HiMeli');
        MetaTag::set('description', 'Except for just developing games, we also provide game art services');
        MetaTag::set('keywords', 'About Himeli, Games, it, technology, gamedev, services, provider, arcade,
        hyper casual, platformer, multiplayer games');
        MetaTag::set('image', asset('img/screen_about_us-min.png'));

        $organization = Schema::organization()
            ->description('Except for just developing games, we also provide game art services')
            ->name('HiMeli')
            ->url( url()->full() )
            ->logo(env('APP_URL') . 'img/' . 'logoB_HiMeli_2020-10.svg')
            ->foundingDate('2016')
            ->sameAs( [
                "https://www.instagram.com/himeliofficial/",
                "https://www.facebook.com/himeliofficial/",
                "https://www.youtube.com/channel/UCRnPEtgvpJqacaaWmvb6WpA",
                "https://twitter.com/himeliofficial"
            ] );

        $person = Schema::person()
            ->name('HiMeli')
            ->url( env('APP_URL') )
            ->sameAs( [
                "https://www.instagram.com/himeliofficial/",
                "https://www.facebook.com/himeliofficial/",
                "https://www.youtube.com/channel/UCRnPEtgvpJqacaaWmvb6WpA",
                "https://twitter.com/himeliofficial"
            ] );

        return view('pages.about_us' , [
            'organization' => $organization,
            'person'       => $person
        ] );
    }
}
