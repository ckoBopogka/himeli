<?php

namespace App\Http\Controllers\Pages;

use App\Post;
use App\Category;
use App\Tag;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use MetaTag;
use Spatie\SchemaOrg\Schema;

class NewsController extends Controller
{
    public function index(): object
    {
        $posts = Post::orderBy('date', 'DESC')
            ->where('status', 0)
            ->get();
        $categories = $this->getCategoriesList();
        $tags = $this->getTagsList();

        MetaTag::set('title', 'News | HiMeli');
        MetaTag::set('description', 'Except for just developing games, we also provide game art services');
        MetaTag::set('keywords',
            'HiMeli News, himeli blog, gamedev, games, vr,
			Games, news, posts, gamedev course, education, technology
		');
        MetaTag::set('image', asset('img/main_banner_3.png'));

        $article = Schema::article()
            ->inLanguage('en')
            ->description('Except for just developing games, we also provide game art services')
            ->datePublished('2021-02-16T13:33:52-07:00')
            ->dateModified('2021-09-10T10:11:47+00:00')
            ->headline('News')
            ->url( url()->full() )
            ->image( asset('img/main_banner_3.png') )
            ->author( [
                '@type' => 'Organization',
                'name' => 'HiMeli Team',
                'sameAs' => 'https://www.instagram.com/himeliofficial/'
            ] )
            ->publisher( [
                '@type' => 'Organization',
                'name' => 'HiMeli',
                'sameAs' => 'https://www.instagram.com/himeliofficial/',
                'logo' => [
                    '@type' => 'ImageObject',
                    'url' => env('APP_URL') . 'img/' . 'logoB_HiMeli_2020-10.svg',
                    'width' => 142,
                    'height' => 62
                ]
            ] );

        $breadcrumbs = Schema::breadcrumbList()
            ->itemListElement([
                Schema::ListItem()
                    ->position(1)
                    ->name('HiMeli')
                    ->item(
                        Schema::Thing()
                            ->setProperty('@id', env('APP_URL'))
                    ),
                Schema::ListItem()
                    ->position(2)
                    ->name('News')
                    ->item(
                        Schema::Thing()
                            ->setProperty('@id', env('APP_URL') . 'news')
                    ),
            ]);

        return view('pages.news', compact( [
            'posts', 'categories', 'tags', 'article', 'breadcrumbs'
        ] ));
    }

    public function showSingle(string $slug): object
    {
        $post = Post::where('slug', $slug)->firstOrFail();

        if ($post->status === 1 && !Auth::check()) {
            abort(404);
        }

        $categories = $this->getCategoriesList();
        $tags = $this->getTagsList();
        $targetPost = $post->getTargetPost();

        MetaTag::set('title', $post->title.' | HiMeli');
        MetaTag::set('description', $post->excerpt);
        MetaTag::set('image', asset($post->getImage()));
        MetaTag::set('keywords', $post->keywords);

        $article = Schema::article()
            ->inLanguage('en')
            ->description($post->excerpt)
            ->datePublished($post->created_at)
            ->dateModified($post->updated_at)
            ->headline($post->title)
            ->url( url()->full() )
            ->image( asset($post->getImage()) )
            ->author( [
                '@type' => 'Organization',
                'name' => 'HiMeli Team',
                'sameAs' => 'https://www.instagram.com/himeliofficial/'
            ] )
            ->publisher([
                '@type' => 'Organization',
                'name' => 'HiMeli',
                'sameAs' => 'https://www.instagram.com/himeliofficial/',
                'logo' => [
                    '@type' => 'ImageObject',
                    'url' => env('APP_URL') . 'img/' . 'logoB_HiMeli_2020-10.svg',
                    'width' => 142,
                    'height' => 62
                ]
            ]);

        $organization = Schema::organization()
            ->description('Except for just developing games, we also provide game art services')
            ->name('HiMeli')
            ->url( url()->full() )
            ->logo(env('APP_URL') . 'img/' . 'logoB_HiMeli_2020-10.svg')
            ->foundingDate('2016')
            ->sameAs( [
                "https://www.instagram.com/himeliofficial/",
                "https://www.facebook.com/himeliofficial/",
                "https://www.youtube.com/channel/UCRnPEtgvpJqacaaWmvb6WpA",
                "https://twitter.com/himeliofficial"
            ] );

        $breadcrumbs = Schema::breadcrumbList()
            ->itemListElement([
                Schema::ListItem()
                    ->position(1)
                    ->name('HiMeli')
                    ->item(
                        Schema::Thing()
                            ->setProperty('@id', env('APP_URL'))
                    ),
                Schema::ListItem()
                    ->position(2)
                    ->name($post->getCategoryTitle())
                    ->item(
                        Schema::Thing()
                            ->setProperty('@id', $post->getCategoryUrl())
                    ),
            ]);

        $post->setCounterView();

        return view('pages.single-news', compact( [
            'post', 'categories', 'tags', 'targetPost', 'article', 'organization', 'breadcrumbs'
        ] ));
    }

    public function getCategoriesList(): object
    {
        return Category::all()->take(11);
    }

    public function getTagsList(): object
    {
        return Tag::all()->take(11);
    }

    public function tagSingle(string $slug): object
    {
        $tag_select = Tag::where('slug', $slug)->firstOrFail();
        $tags = $this->getTagsList();
        $categories = $this->getCategoriesList();
        $posts = [];

        if (isset($tag_select->posts)) {
            $posts = $tag_select->posts->whereIn('status', 0)->sortByDesc('created_at');
        }

        MetaTag::set('title', $tag_select->title.' | Tag | HiMeli');
        MetaTag::set('description', 'We develop interesting and educational games every day');
        MetaTag::set('image', asset('img/logoS_256px_HiMeli_2020.png'));

        $breadcrumbs = Schema::breadcrumbList()
            ->itemListElement([
                Schema::ListItem()
                    ->position(1)
                    ->name('HiMeli')
                    ->item(
                        Schema::Thing()
                            ->setProperty('@id', env('APP_URL'))
                    ),
                Schema::ListItem()
                    ->position(2)
                    ->name($tag_select->title)
                    ->item(
                        Schema::Thing()
                            ->setProperty('@id', env('APP_URL') . 'tag/' . $tag_select->slug)
                    ),
            ]);

        return view('pages.tag', compact( [
            'tag_select', 'posts', 'tags', 'categories', 'breadcrumbs'
        ]));
    }

    public function categorySingle(string $slug): object
    {
        $category_select = Category::where('slug', $slug)->firstOrFail();
        $tags = $this->getTagsList();
        $categories = $this->getCategoriesList();
        $posts = [];

        if (isset($category_select->id)) {
            $posts = Post::orderBy('date', 'DESC')
                ->where('status', 0)
                ->where('category_id', $category_select->id)
                ->get();
        }

        MetaTag::set('title', $category_select->title.' | Categories | HiMeli');
        MetaTag::set('description', 'We develop interesting and educational games every day');
        MetaTag::set('image', asset('img/logoS_256px_HiMeli_2020.png'));

        $breadcrumbs = Schema::breadcrumbList()
            ->itemListElement([
                Schema::ListItem()
                    ->position(1)
                    ->name('HiMeli')
                    ->item(
                        Schema::Thing()
                            ->setProperty('@id', env('APP_URL'))
                    ),
                Schema::ListItem()
                    ->position(2)
                    ->name($category_select->title)
                    ->item(
                        Schema::Thing()
                            ->setProperty('@id', env('APP_URL') . 'category/' . $category_select->slug)
                    ),
            ]);

        return view('pages.category', compact( [
            'category_select', 'posts', 'tags', 'categories', 'breadcrumbs'
        ] ));
    }

    /**
     * Ajax request for Home Page with News
     * @param  Request  $request
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function loadAjaxPosts(Request $request)
    {
        $slug = $request->get('category');
        $category_select = Category::where('slug', $slug)->firstOrFail();

        if (isset($category_select->id)) {
            $posts = Post::orderBy('date', 'DESC')
                ->where('status', 0)
                ->where('category_id', $category_select->id)
                ->take(4)
                ->get();

            $list_items = view('pages.parts.ajaxItemNews', ['posts' => $posts]);

            return $list_items;
        }
    }
}
