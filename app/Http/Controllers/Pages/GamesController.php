<?php

namespace App\Http\Controllers\Pages;

use App\Game;
use App\Category;
use App\Post;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use MetaTag;
use Spatie\SchemaOrg\Schema;

class GamesController extends Controller
{
    public function index()
    {
        MetaTag::set('title', 'All Games | HiMeli');
        MetaTag::set('description', 'Except for just developing games, we also provide game art services');
		MetaTag::set('keywords', 'All games, popular games, mobile games, gamedev, interesting games, coming games');
        MetaTag::set('image', asset('img/Group 3-min.png'));

        $games = Game::all()
            ->whereIn('status', 0)
            ->whereIn('is_coming_soon', 0)
            ->sortByDesc('date');

        $games_popular = Game::all()
            ->whereIn('status', 0)
            ->whereIn('is_popular', 1)
            ->whereIn('is_coming_soon', 0)
            ->sortByDesc('date');

        $games_coming_soon = Game::all()
            ->whereIn('status', 0)
            ->whereIn('is_coming_soon', 1)
            ->sortByDesc('date');

        $organization = Schema::organization()
            ->description('Except for just developing games, we also provide game art services')
            ->name('HiMeli')
            ->url( url()->full() )
            ->logo(env('APP_URL') . 'img/' . 'logoB_HiMeli_2020-10.svg')
            ->foundingDate('2016')
            ->sameAs( [
                "https://www.instagram.com/himeliofficial/",
                "https://www.facebook.com/himeliofficial/",
                "https://www.youtube.com/channel/UCRnPEtgvpJqacaaWmvb6WpA",
                "https://twitter.com/himeliofficial"
            ] );

        return view('pages.games', [
            'games'             => $games,
            'games_popular'     => $games_popular,
            'games_coming_soon' => $games_coming_soon,
            'organization'      => $organization
        ]);
    }

    public function showSingle( string $slug) : object
    {
        $post = Game::where('slug', $slug)->firstOrFail();
        $news = $this->getNewsGame($slug) ?? null;

        if ( $post->status === 1 && ! Auth::check() ){
             abort(404);
        }

        MetaTag::set('title', $post->title .' | HiMeli');
        MetaTag::set('description', $post->except);
		MetaTag::set('keywords', $post->keywords);
        MetaTag::set('image', asset('uploads/'.$post->thumbnail_game));

        $article = Schema::article()
            ->inLanguage('en')
            ->description($post->excerpt)
            ->datePublished($post->created_at)
            ->dateModified($post->updated_at)
            ->headline($post->title)
            ->url( url()->full() )
            ->image( asset('uploads/'.$post->thumbnail_game) )
            ->author( [
                '@type' => 'Organization',
                'name' => 'HiMeli Team',
                'sameAs' => 'https://www.instagram.com/himeliofficial/'
            ] )
            ->publisher([
                '@type' => 'Organization',
                'name' => 'HiMeli',
                'sameAs' => 'https://www.instagram.com/himeliofficial/',
                'logo' => [
                    '@type' => 'ImageObject',
                    'url' => env('APP_URL') . 'img/' . 'logoB_HiMeli_2020-10.svg',
                    'width' => 142,
                    'height' => 62
                ]
            ]);

        $breadcrumbs = Schema::breadcrumbList()
            ->itemListElement([
                Schema::ListItem()
                    ->position(1)
                    ->name('HiMeli')
                    ->item(
                        Schema::Thing()
                            ->setProperty('@id', env('APP_URL'))
                    ),
                Schema::ListItem()
                    ->position(2)
                    ->name('Games')
                    ->item(
                        Schema::Thing()
                            ->setProperty('@id', env('APP_URL') . 'games')
                    ),
            ]);

        $organization = Schema::organization()
            ->description('Except for just developing games, we also provide game art services')
            ->name('HiMeli')
            ->url( url()->full() )
            ->logo(env('APP_URL') . 'img/' . 'logoB_HiMeli_2020-10.svg')
            ->foundingDate('2016')
            ->sameAs( [
                "https://www.instagram.com/himeliofficial/",
                "https://www.facebook.com/himeliofficial/",
                "https://www.youtube.com/channel/UCRnPEtgvpJqacaaWmvb6WpA",
                "https://twitter.com/himeliofficial"
            ] );

        return view('pages.single-game', compact([
            'post', 'news', 'article', 'breadcrumbs', 'organization'
        ]) );
    }

    private function getNewsGame( string $slug)
	{
		$category = Category::where('slug', $slug)->first();

		if ( isset($category->id) ) {
			$news = Post::orderBy('date', 'DESC')
				->where('status', 0)
				->where('category_id', $category->id)
				->take(4)
				->get();

			return $news;
		}
	}
}
