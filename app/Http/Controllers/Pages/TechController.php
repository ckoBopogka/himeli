<?php

namespace App\Http\Controllers\Pages;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TechController extends Controller
{
    public function privacy_policy() : object
	{

		return view('pages.tech.privacy_policy');
	}
}
