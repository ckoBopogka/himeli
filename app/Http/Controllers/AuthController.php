<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;
use MetaTag;

class AuthController extends Controller
{
    public function registerForm()
    {
        MetaTag::set('title', 'Registration | HiMeli');
        MetaTag::set('description', 'We develop interesting and educational games every day');
        MetaTag::set('image', asset('img/logoS_256px_HiMeli_2020.png'));

        return view('pages.register');
    }

    public function register(Request $request)
    {
        MetaTag::set('title', 'Ukrainian GameDev Company | HiMeli');
        MetaTag::set('description', 'We develop interesting and educational games every day');
        MetaTag::set('image', asset('img/logoS_256px_HiMeli_2020.png'));

        $this->validate( $request, [
            'name'     => 'required',
            'email'    => 'required|email|unique:users',
            'password' => 'required'
        ]);

        $user = User::add( $request->all() );
        $user->generatePassword($request->get('password'));

        return redirect('/login');
    }

    public function loginForm()
    {
        MetaTag::set('title', 'Login | HiMeli');
        MetaTag::set('description', 'We develop interesting and educational games every day');
        MetaTag::set('image', asset('img/logoS_256px_HiMeli_2020.png'));

        return view('pages.login');
    }

    public function login( Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if (Auth::attempt([
           'email' => $request->get('email'),
           'password' => $request->get('password')
        ])) {
            return redirect('/admin/');
        } else {
            return redirect()->back()->with('status', 'Not correct login or password');
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }
}
