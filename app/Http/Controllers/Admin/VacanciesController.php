<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Vacancy;
use Illuminate\Http\Request;

class VacanciesController extends Controller
{
    public function index()
    {
        $vacancies = Vacancy::orderBy('date', 'DESC')->get();
        return view( 'admin.vacancies.index', ['vacancies' => $vacancies] );
    }

    public function create()
    {
        return view('admin.vacancies.create');
    }

    public function store( Request $request )
    {
        $this->validate($request, [
            'title' => 'required',
            'content' => 'required',
            'date' => 'required',
        ]);

        $vacancy = Vacancy::add($request->all());
        $vacancy->toggleStatus($request->get('status'));
        $vacancy->toggleHots($request->get('is_hot'));

        return redirect()->route('vacancies.index');
    }

    public function edit( $id )
    {
        $vacancy = Vacancy::find($id);
        return view('admin.vacancies.edit', compact('vacancy') );
    }

    public function update( Request $request, $id )
    {
        $this->validate( $request, [
            'title' => 'required',
            'content' => 'required',
            'date' => 'required',
        ] );

        $vacancy = Vacancy::find($id);
        $vacancy->edit($request->all());
        $vacancy->toggleStatus($request->get('status'));
        $vacancy->toggleHots($request->get('is_hot'));

        return redirect()->route('vacancies.index');
    }

    public function destroy( $id )
    {
        Vacancy::find($id)->remove();
        return redirect()->route('vacancies.index');
    }
}
