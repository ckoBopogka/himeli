<?php

namespace App\Http\Controllers\Admin;

use App\Game;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GamesController extends Controller
{
    public function index()
    {
        $games = Game::all()->sortByDesc('date');
        return view('admin.games.index', ['games' => $games] );
    }

    public function create()
    {
        return view('admin.games.create');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title'            => 'required',
            'content'          => 'required',
            'except'           => 'required',
            'date'             => 'required',
            'banner_game'      => 'nullable|image',
            'thumbnail_game'   => 'nullable|image'
        ]);

        $game = Game::add( $request->all() );
        $game->uploadImage( $request->file('thumbnail_game') );
        $game->uploadBanner( $request->file('banner_game') );
        $game->toggleStatus( $request->get('status') );
        $game->toggleFeatured( $request->get('is_featured') );
        $game->toggleComingSoon( $request->get('is_coming_soon') );
        $game->togglePopular( $request->get('is_popular') );
        $game->toggleTop( $request->get('is_top') );
        $game->uploadGallery( $request->file('files_gallery') );

        return redirect()->route('posts-game.index');
    }

    public function edit($id)
    {
        $game = Game::find($id);
        return view( 'admin.games.edit', ['game' => $game] );
    }

    public function update(Request $request, $id)
    {
        $this->validate( $request, [
            'title'            => 'required',
            'content'          => 'required',
            'except'           => 'required',
            'date'             => 'required',
            'banner_game'      => 'nullable|image',
            'thumbnail_game'   => 'nullable|image',
			'slug'    		   => 'regex:/^[a-zA-Z0-9()*_\-!#$%^&*,."\'\][]+$/i'
        ] );

        $game = Game::find( $id );
        $game->edit( $request->all() );
        $game->uploadImage( $request->file('thumbnail_game') );
        $game->uploadBanner( $request->file('banner_game') );
        $game->toggleStatus( $request->get('status') );
        $game->toggleFeatured( $request->get('is_featured') );
        $game->toggleComingSoon( $request->get('is_coming_soon') );
        $game->togglePopular( $request->get('is_popular') );
        $game->toggleTop( $request->get('is_top') );
        $game->uploadGallery( $request->file('files_gallery') );

        return redirect()->route('posts-game.index');
    }

    public function destroy($id)
    {
        Game::find($id)->remove();
        return redirect()->route('posts-game.index');
    }
}
