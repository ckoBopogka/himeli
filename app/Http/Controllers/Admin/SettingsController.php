<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Setting;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    public function index() : object
    {
        $options = Setting::all();
        return view('admin.settings', ['options' => $options]);
    }

    public function store( Request $request )
    {
        $save_settings = $request->only('options_name');
        Setting::add($save_settings);

        return redirect()->route('settings.index')->with('status', 'Настройки успешно сохранены');
    }
}
