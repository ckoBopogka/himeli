<?php

namespace App\Http\Controllers\Admin\includes\tinyMCE;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class MediaController extends Controller
{
	public function store(Request $request)
	{
		$this->saveImg( $request->file('file') );
    }

    public function saveImg( $image )
	{
		if ( $image === null ) {
			return;
		}

		$location_upload = $this->getUploadFolder();

		$filename = Str::random(10) . '.' . $image->extension();
		$image->storeAs($location_upload['path'], $filename);

		$request = [
			'filename' => $filename,
			'location'=> $location_upload['url'] . $filename
 		];

		$request = json_encode($request);

		echo $request;
		die();
	}

	private function getUploadFolder() : array
	{
		$current_date = date('y m');
		$public_path  = public_path() . '/uploads/other/';
		$date_array   = explode(' ', $current_date);
		$year_path    = $public_path . $date_array[0];
		$month_path   = $public_path . $date_array[0] . '/' . $date_array[1];

		$path_folder  = 'uploads/other/' . $date_array[0] . '/' . $date_array[1];
		$url_folder   = '/uploads/other/' . $date_array[0] . '/' . $date_array[1] .'/';

		if ( ! is_dir($year_path) ) {
			mkdir($year_path, 0775);
		}

		if ( ! is_dir($month_path) ) {
			mkdir($month_path, 0775);
		}

		$location_upload = [
			'path' => $path_folder,
			'url'  => $url_folder
		];

		return $location_upload;
	}

}
