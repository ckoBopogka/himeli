<?php

namespace App\Http\Controllers\Admin\includes\telegramAPI;

use App\Http\Controllers\Controller;
use App\Post;
use Illuminate\Http\Request;

class TelegramController extends Controller
{
    private $message;
    private $chat_id;
    private $id_telegram_token;

    /**
     *  Redirection at posts.index after request
     * @param $id - Shared post ID
     * @return \Illuminate\Http\RedirectResponse
     */
    public function index( $id )
    {
        $this->getTelegramKeys();
        $this->getPostContent($id);
        $this->sendRequestToAPI();

        return redirect()->route('posts.index')->with('status', 'Новость была опубликована в HiMeli чате');
    }

    /**
     * Get TELEGRAM_CHAT_ID and TELEGRAM_TOKEN from env file
     * @return \Illuminate\Http\RedirectResponse|void
     */
    private function getTelegramKeys()
    {
        if ( env('TELEGRAM_CHAT_ID') === null || empty(env('TELEGRAM_CHAT_ID')) ) {
            return redirect()->route('posts.index')->with('status', 'Новость не была опубликована в HiMeli чате. Не корректный Telegram Chat ID');
        }

        if ( env('TELEGRAM_TOKEN') === null || empty(env('TELEGRAM_TOKEN')) ) {
            return redirect()->route('posts.index')->with('status', 'Новость не была опубликована в HiMeli чате. Не корректный Telegram Bot Token');
        }

        $this->chat_id = env('TELEGRAM_CHAT_ID');
        $this->id_telegram_token = env('TELEGRAM_TOKEN');
    }

    /**
     * Get content from Post and create message
     * @param  int  $id
     */
    private function getPostContent( int $id ): void
    {
        $post = Post::find( $id );
        $url = env('APP_URL') . $post->the_slug();

        $this->message = 'Всем привет! 😎 Сегодня опубликовали новую статью на нашем сайте 👀 : <a href="' . $url . '?utm_source=telegram&utm_medium=social">' . $post->title . '</a> Делитесь в соц. сетях и рассказывайте друзьям! 🤠';
    }

    /**
     * Send request into Telegram API
     */
    private function sendRequestToAPI(): void
    {
        $data     = [
            'chat_id'    => $this->chat_id,
            'parse_mode' => 'HTML',
            'text'       => stripslashes( $this->message )
        ];

        file_get_contents( "https://api.telegram.org/" . $this->id_telegram_token . "/sendMessage?" . http_build_query( $data ) );
    }

}
