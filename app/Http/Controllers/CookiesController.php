<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;

class CookiesController extends Controller
{
    /**
     * @param  Request  $request
     */
    public function index(Request $request) : void
    {
        Cookie::queue('himeli-cookie-agree', 'himeli-cookie-agree', 10000 );
    }
}
