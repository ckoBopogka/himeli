<?php

namespace App\Http\Controllers;

use App\Game;
use App\Post;
use App\Category;
use App\Tag;
use Illuminate\Http\Request;

class SitemapController extends Controller
{
	public static function index() : object
	{
		$game = Game::orderBy('date', 'DESC')
			->where('status', 0)
			->first();

		$post = Post::orderBy('date', 'DESC')
			->where('status', 0)
			->first();

		$category = Category::orderBy('updated_at', 'DESC')
			->first();

		$tag = Tag::orderBy('updated_at', 'DESC')
			->first();

		return response()->view('sitemap.index', [
			'game'     => $game,
			'post' 	   => $post,
			'category' => $category,
			'tag' 	   => $tag
		])->header('Content-Type', 'text/xml');
	}

	public function posts() : object
	{
		$posts = Post::orderBy('date', 'DESC')
			->where('status', 0)
			->get();

		return response()->view('sitemap.posts', [
			'posts' => $posts
		])->header('Content-Type', 'text/xml');
	}

	public function categories() : object
	{
		$categories = Category::orderBy('updated_at', 'DESC')->get();

		return response()->view('sitemap.categories', [
			'categories' => $categories
		])->header('Content-Type', 'text/xml');
	}

	public function tags() : object
	{
		$tags = Tag::orderBy('updated_at', 'DESC')->get();

		return response()->view('sitemap.tags', [
			'tags' => $tags
		])->header('Content-Type', 'text/xml');
	}

	public function games() : object
	{
		$games = Game::orderBy('updated_at', 'DESC')->get();

		return response()->view('sitemap.games', [
			'games' => $games
		])->header('Content-Type', 'text/xml');
	}
}
