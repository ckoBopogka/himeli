<?php

namespace App\Http\Controllers;

use App\Category;
use App\Game;
use App\Post;
use Illuminate\Http\Request;
use MetaTag;
use Spatie\SchemaOrg\Schema;

class HomeController extends Controller
{
    public function index() : object
    {
        MetaTag::set('title', 'GameDev Team | HiMeli');
        MetaTag::set('description', 'Except for just developing games, we also provide game art services');
		MetaTag::set('keywords', 'HiMeli, mobile games, gamedev, entertainment, games for children, software, indie team');
        MetaTag::set('image', asset('img/main_banner_3.png'));

        $organization = Schema::organization()
            ->description('Except for just developing games, we also provide game art services')
            ->name('HiMeli')
            ->url( url()->full() )
            ->logo(env('APP_URL') . 'img/' . 'logoB_HiMeli_2020-10.svg')
            ->foundingDate('2016')
            ->sameAs( [
                "https://www.instagram.com/himeliofficial/",
                "https://www.facebook.com/himeliofficial/",
                "https://www.youtube.com/channel/UCRnPEtgvpJqacaaWmvb6WpA",
                "https://twitter.com/himeliofficial"
            ] );

        $top_game = Game::orderBy('date', 'DESC')
            ->where('status', 0)
            ->where('is_top', 1)
            ->where('is_coming_soon', 0)
            ->first();

        $latest_game = Game::orderBy('date', 'DESC')
            ->where('status', 0)
            ->where('is_coming_soon', 0)
            ->get();

        $posts      = $this->getHomeNewsCat();
        $categories = $this->getCategoriesList();

        return view('pages.index', [
            'top_game'     => $top_game,
            'latest_game'  => $latest_game,
            'posts'        => $posts,
            'categories'   => $categories,
            'organization' => $organization
        ]);
    }

    private function getCategoriesList() : object
    {
        return Category::all()->take(4);
    }

    private function getHomeNewsCat() : object
    {
        $category_news = Category::where('slug', 'news')->first();

        if ( isset($category_news->id) ){
			$posts = Post::orderBy('date', 'DESC')
				->where('status', 0)
				->where('category_id', $category_news->id)
				->take(4)
				->get();
		} else{
			$posts = (object)[];
		}

		return $posts;
    }
}
