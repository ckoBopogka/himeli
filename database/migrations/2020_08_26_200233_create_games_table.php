<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateGamesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('games', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string('slug');
            $table->text('content');
            $table->text('except');
            $table->date('date')->nullable();
            $table->string('thumbnail_game')->nullable();
            $table->string('banner_game')->nullable();
            $table->text('play_market_link')->nullable();
            $table->text('app_store_link')->nullable();
            $table->text('forum_link')->nullable();
            $table->integer('category_game_id')->nullable();
            $table->string('files_gallery')->nullable();
            $table->integer('user__game_id')->nullable();
            $table->integer('status')->default(0);
            $table->integer('is_featured')->default(0);
            $table->integer('is_top')->default(0);
            $table->integer('is_coming_soon')->default(0);
            $table->integer('is_popular')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('games');
    }
}
