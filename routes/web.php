<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/clear', function() {
    Artisan::call('cache:clear');
    Artisan::call('config:clear');
    Artisan::call('route:clear');
    Artisan::call('view:clear');

    return "Cache is cleared";
});

Route::get( '/', 'HomeController@index' )->name('home');
Route::get( '/games/', 'Pages\GamesController@index' )->name('all_games');
Route::get( '/games/{slug}/', 'Pages\GamesController@showSingle' );

Route::get( '/news/', 'Pages\NewsController@index' )->name('all_news');
Route::get( '/news/{slug}/', 'Pages\NewsController@showSingle' );
Route::get( '/tag/{slug}', 'Pages\NewsController@tagSingle' )->name( 'tag.show' );
Route::get( '/category/{slug}', 'Pages\NewsController@categorySingle' )->name( 'category.show' );

Route::post( '/subscribe', 'SubController@subscribe' );
Route::get( '/verify/{token}', 'SubController@verify' );

Route::group(['middleware' => 'auth' ], function () {
    Route::get( '/profile', 'ProfileController@index' );
    Route::post( '/profile', 'ProfileController@store' );
    Route::get( '/logout', 'AuthController@logout' );
});

Route::group( ['middleware' => 'guest'], function () {
//    Route::get( '/register', 'AuthController@registerForm' );
//    Route::post( '/register', 'AuthController@register' );
    Route::get( '/login', 'AuthController@loginForm' )->name( 'login' );
    Route::post( '/login', 'AuthController@login' );
});

Route::group( ['prefix'=>'admin', 'namespace'=>'Admin', 'middleware' => 'admin'], function () {
    Route::get( '/', 'DashboardController@index' );
    Route::get( '/telegramAPI/{id}', 'includes\telegramAPI\TelegramController@index' )
        ->name('telegramAPI.send_request');
    Route::resource( '/categories', 'CategoriesController' );
    Route::resource( '/tags', 'TagsController' );
    Route::resource( '/pages', 'PagesController' );
    Route::resource( '/users', 'UsersController' );
    Route::resource( '/posts', 'PostsController' );
    Route::resource( '/posts-game', 'GamesController' );
    Route::resource( '/subscribers', 'SubscribersController' );
    Route::resource( '/settings', 'SettingsController' );
    Route::resource( '/vacancies', 'VacanciesController' );
});

/**
 * Ajax requests
 */
Route::post( '/admin/img/mcedit', 'Admin\includes\tinyMCE\MediaController@store' )->name( 'admin.mcedit' );
Route::post( '/News/ajaxPOST', 'Pages\NewsController@loadAjaxPosts' )->name( 'ajax.getFourPosts' );
Route::post( '/auth/cookies', 'CookiesController@index' )->name( 'ajax.setCookies' );

/**
 * Sitemaps
 */
Route::get('/sitemap', 'SitemapController@index');
Route::get('/sitemap/posts', 'SitemapController@posts');
Route::get('/sitemap/pages', 'SitemapController@pages');
Route::get('/sitemap/categories', 'SitemapController@categories');
Route::get('/sitemap/tags', 'SitemapController@tags');
Route::get('/sitemap/games', 'SitemapController@games');

/**
 * Static page
 */
Route::get('/privacy_policy', 'Pages\TechController@privacy_policy')->name('privacy_policy');
Route::get('/about_us/', 'Pages\AboutUsController@index')->name('about_us');
Route::get('/careers', 'Pages\CareersController@render_page')->name('careers');
Route::post( '/resume', 'Pages\CareersController@send_resume' )->name('send_resume');
