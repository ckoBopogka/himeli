/* Version: 1.0.1 */

/**
 *  Init slick slider with configuration
 */
(function () {
    jQuery('.slider-list-wrapper-feature-slider').slick({
        slidesToShow: 3,
        dots: true,
        infinite: true,
        arrows: true,
        autoplay: true,
        cssEase: 'linear',
        slidesToScroll: 1,
        prevArrow: '<button type="button" class="slick-prev"><svg width="32" height="38" viewBox="0 0 32 38" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M32 19L0.499998 37.1865L0.5 0.813466L32 19Z" /></svg></button>',
        nextArrow: '<button type="button" class="slick-next"><svg width="32" height="38" viewBox="0 0 32 38" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M32 19L0.499998 37.1865L0.5 0.813466L32 19Z" /></svg></button>',
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    arrows: false
                }
            },
            {
                breakpoint: 1000,
                settings: {
                    slidesToShow: 2,
                    arrows: false
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    arrows: false
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    arrows: false
                }
            },
        ]
    })

    jQuery('.list-screen-game').slick({
        centerMode: true,
        // centerPadding: '120px',
        cssEase: 'linear',
        slidesToShow: 3,
        autoplay: true,
        dots: true,
        infinite: true,
        responsive: [
            {
                breakpoint: 1200,
                settings: {
                    arrows: false
                }
            },
            {
                breakpoint: 1000,
                settings: {
                    slidesToShow: 2,
                    arrows: false
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    arrows: false
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 1,
                    arrows: false
                }
            },
        ]
    })
})();

/**
 * Init Galery with settings
 */
(function () {
    jQuery(document).ready(function(){
        jQuery("a[rel^='prettyPhoto']").prettyPhoto({
            show_title: false,
            deeplinking: false,
            horizontal_padding: 0,
            ie6_fallback: false,
            social_tools: false,
            slideshow: false,
            keyboard_shortcuts: false,
            modal: false
        })
    })
})();

/**
 * Select tabs for news banner (Home page)
 */
(function () {
    jQuery('.box--item-tabs').click(function (e) {
        e.preventDefault()
        if ( !jQuery(this).hasClass('current') ) {
            jQuery('.box--item-tabs').removeClass('current')
            jQuery(this).addClass('current')
        }
    })
})();

(function(){

    document.addEventListener('click', (e) => {
        let event = e.target

        if ( event.classList.contains('item-tabs--link') ) {
            e.preventDefault()
            let category = event.getAttribute('data-source-cat')
            let list_tabs = document.querySelector('.box--list-tabs')

            if (typeof list_tabs === 'undefined' || list_tabs === null || typeof category === 'undefined') {
                return false;
            }

            const config = {
                request : "/News/ajaxPOST",
                token : list_tabs.getAttribute('data-token')
            }

            const loadingPost = async (category) => {
                let data = new FormData()
                data.append('_token', config.token)
                data.append('category', category)

                let response = await fetch(config.request,
                    {
                        method: "POST",
                        body: data
                    })

                if (response.status === 200) {
                    let data = await response.text()
                    renderNewsPosts(data)
                }
            }

            const renderNewsPosts = (data) => {
                let container = document.querySelector('.list-items-current-news')
                container.innerHTML = data
            }

            loadingPost(category)
        }
    })

})();

/**
 * Display Search
 */
(function(){
    jQuery('.search-btn').click(function (e) {
        e.preventDefault()
        jQuery('.search-form').toggleClass('open')
    })

    jQuery('.btn--close-search-form').click(function (e) {
        e.preventDefault()
        jQuery('.search-form').toggleClass('open')
    })
})();


/**
 * Tabs for Careers
 */
(function(){
    jQuery('.item-vacancy').click(function (e) {
        jQuery(this).toggleClass('opened')

        jQuery('html, body').animate({
            scrollTop: jQuery(this).offset().top - jQuery('.main-header').height() - 10
        }, 700);
    })
})();

(function (){
    jQuery('#file_resume').change( (e) => {
        e.preventDefault()
        jQuery('#selected_filename').text(jQuery('#file_resume')[0].files[0].name)
        jQuery('.loaded_file').removeClass('none')
        jQuery('.container-file label').addClass('none')
    });
    jQuery('#remove_file').click( (e) => {
        e.preventDefault()
        jQuery('#file_resume').val('')
        jQuery('#selected_filename').empty()
        jQuery('.loaded_file').addClass('none')
        jQuery('.container-file label').removeClass('none')
    });
    jQuery('#close_popup').click( (e) => {
        e.preventDefault()
        jQuery('.status-popup-message').addClass('none')
    });
})();
