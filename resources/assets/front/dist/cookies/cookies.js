(function () {
    document.addEventListener('DOMContentLoaded', () => {

        let cookie_popup = document.querySelector('.cookies')

        if (typeof cookie_popup === 'undefined' || cookie_popup === null){
            return false;
        }

        let cookieAgree = document.querySelector('.accept-cookies')
        let cookieReject = document.querySelector('.reject-cookies')

        if (cookieAgree && cookieReject) {
            cookieAgree.addEventListener('click', () => {
                save_cookies()
            })

            cookieReject.addEventListener('click', () => {
                cookie_popup.style.display = 'none'
            })
        }

        const config = {
            request : "/auth/cookies",
            token : cookie_popup.getAttribute('data-token-cookies')
        }

        const save_cookies = async () => {
            let data = new FormData()
            data.append('_token', config.token)

            let response = await fetch(config.request,
                {
                    method: "POST",
                    body: data
                })

            if (response.status === 200) {
                cookie_popup.style.display = 'none'
            }
        }

    });

} )();
