$(document).ready(function (){

    /**
     * Init tinyMCE editor
     */

    const config = {
        image_path : "/admin/img/mcedit"
    };

    tinymce.init({
        selector:'textarea#content',
        plugins: [
            'advlist autolink lists link image charmap print preview anchor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table paste imagetools wordcount'
        ],
        file_picker_types: 'file image media',
        toolbar: 'numlist bullist link image',

        images_upload_handler: (blobInfo, success, failure) => {
            let xhr, formData;

            xhr = new XMLHttpRequest();
            xhr.withCredentials = false;

            xhr.open('POST', config.image_path);

            let csrf = document.querySelector('input[name="_token"]').value;
            xhr.setRequestHeader("X-CSRF-Token", csrf);

            xhr.onload = () => {
                let json;

                if (xhr.status != 200) {
                    console.log('Eep - it ', xhr.status);
                    failure('HTTP Error: ' + xhr.status);
                    return;
                }

                console.log(xhr.response);
                json = JSON.parse(xhr.responseText);

                if ( ! json || typeof json.location != 'string') {
                    console.log('Here');
                    failure('Invalid JSON: ' + xhr.responseText);
                    return;
                }
                success(json.location);
            };

            formData = new FormData();

            formData.append('file', blobInfo.blob(), blobInfo.filename());
            xhr.send(formData);
        },
    });

    /**
     * Init Datepicker
     */
    $(".select2").select2();

    $('#datepicker').datepicker({
        autoclose: true,
        format: 'dd.mm.yy'
    });

    /**
     * Select today date for create post/game
     */
    if ( $('.content-wrapper').hasClass('create-post') || $('.content-wrapper').hasClass('create-game') ) {
        $("#datepicker").datepicker().datepicker("setDate", new Date());
    }

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass: 'iradio_minimal-blue'
    });


    /**
     * Choose files for Gallery
     **/
    let inputs = document.querySelectorAll('.inputfiles');
    Array.prototype.forEach.call(inputs, function(input){
        let label	 = input.nextElementSibling,
            labelVal = label.innerHTML,
            span     = label.nextElementSibling;

        input.addEventListener('change', function(e){
            let fileName = '';

            if( this.files && this.files.length > 1 ) {
                fileName = ( this.getAttribute( 'data-multiple-caption' ) || '' ).replace( '{count}', this.files.length );
            } else {
                fileName = e.target.value.split( '\\' ).pop();
            }

            if( fileName ) {
                label.innerHTML = fileName;
            } else {
                label.innerHTML = labelVal;
            }
        });
    });
});

/**
 * Events for settings tabs
 */
(function (){
    document.addEventListener('DOMContentLoaded', () => {
        document.addEventListener('click', (event) => {
            let tab = event.target;

            if ( tab.getAttribute('data-tab') === null ){
                return;
            }

            activate_tab( tab.getAttribute('data-tab') );
        });

        const activate_tab = ( tab ) => {
            document.querySelector('.tab-bar-item.active').classList.remove('active');
            document.querySelector('.tab-item.active').classList.remove('active');

            document.querySelector('.'+tab).classList.add('active');
            document.getElementById(tab).classList.add('active');
        };

    });
})();
