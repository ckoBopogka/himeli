@include('pages.parts.head')

@include('pages.parts.header')

<section class="top-banner-page">
    <div class="box-banner">
        <ul class="list-banners">
            <li class="item-banners">
                <img src="/img/screen_about_us-min.png" alt="About Us screen">
            </li>
        </ul>
    </div>
</section>

<section class="top-game-banner top_block">
    <div class="content-block">
        <div class="shadow-container">
            <div class="information_top_game">
                <div class="title--information_top_game">
                    <div class="title_game">
                        <h2>About Us</h2>
                    </div>
                </div>
                <div class="grid_70x30_block">
                    <div class="content">
                        <p>It all started in 2016 when a team of enthusiastic software developers decided to create their first game. At that time, this desire was just an idea that wasn’t fulfilled until we found one more eager specialist in 2018. Today, Himeli is a growing team of developers and testers that aren’t going to stop developing.</p>
                        <br>
                        <p>Currently, we are working with the following genres: Arcade, Hyper Casual, Platformer, Multiplayer Games. Except for just developing games, we also provide game art services by creating new innovative ideas and progressive concepts.</p>
                        <br>
                        <p>Himeli is all about quality. Whatever the purpose of our client is, we are ready to meet its requirements so that we can show the best possible result. To do that, we try to implement the most effective methods of work that will be useful in specific cases. We always pay attention to our clients’ needs and special requirements because each of our specialists has his own field of specialization.</p>
                        <br>
                        <p>In general, we are proud of our team and the quality of our work. But we won’t stop improving ourselves because the sky is the limit!</p>
                    </div>
                    <div class="himeli_logo_about_us">
                        <img src="/img/logoB_HiMeli_2020-10.svg" alt="GameDev HiMeli">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@include('pages.parts.footer')
