@include('pages.parts.head')

@include('pages.parts.header')

<h1 style="display: none;">All Games</h1>

<section class="top-banner-page">
    <div class="box-banner">
        <ul class="list-banners">
            <li class="item-banners">
                <img src="/img/Group 3-min.png" alt="All Games" width="1920" height="400">
            </li>
        </ul>
    </div>
</section>

@if ($games_coming_soon->count() > 0)
    <section class="list-games" id="comming-soon">
        <div class="mini-title-box">
            <div class="container">
                <div class="row">
                    <h3 class="float-text-right">Coming soon</h3>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="games--list-game">
                    @foreach($games_coming_soon as $game_coming_soon)
                        <div class="item-game">
                            <div class="thumbnail--item-game">
                                <img src="/uploads/{{$game_coming_soon->thumbnail_game}}" alt="{{$game_coming_soon->title}}" width="351" height="351">
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
@endif

@if ($games->count() > 0)
    <section class="list-games" id="all-games">
        <div class="mini-title-box">
            <div class="container">
                <div class="row">
                    <h3 class="float-text-left">All games</h3>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="games--list-game">
                    @foreach($games as $game)
                        <div class="item-game" style="background-image: url(/uploads/{{$game->thumbnail_game}})" title="{{$game->title}}">
                            <div class="item-menu-feature-slider">
                                <div class="item-menu-current">
                                    <a href="/games/{{$game->slug}}">More</a>
                                </div>
                                @isset($late_game->forum_link)
                                    <div class="item-menu-current">
                                        <a href="{{$late_game->forum_link}}">Forum</a>
                                    </div>
                                @endisset
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
@endif

@include('pages.parts.footer')
