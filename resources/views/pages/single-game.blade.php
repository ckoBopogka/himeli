@include('pages.parts.head')

@include('pages.parts.header')

<section class="top-banner-page">
    <div class="box-banner">
        <ul class="list-banners">
            <li class="item-banners">
                <img src="/uploads/{{$post->banner_game}}" alt="{{$post->title}}" width="1920" height="700">
            </li>
        </ul>
    </div>
</section>

<section class="top_content_game">
    <div class="row-delete">
        <div class="container">
            <div class="box--top_content_game">
                <div class="left--top_content_game">
                    <div class="main-thumbnail">
                        <img src="/uploads/{{$post->thumbnail_game}}" alt="{{$post->title}}" width="430" height="300">
                    </div>
                    <div class="links-download">
                        @if ($post->app_store_link)
                            <div class="app_store_top_game item-download">
                                <a href='{{$post->app_store_link}}' target="_blank" aria-label="App Store" rel="noreferrer">
                                    <img src="/img/app_store_badge.svg" alt="App Store" width="121" height="35">
                                </a>
                            </div>
                        @endif
                        @if ($post->play_market_link)
                            <div class="google_play_top_game item-download">
                                <a href='{{$post->play_market_link}}' target="_blank" aria-label="Google Play" rel="noreferrer">
                                    <img src="/img/google_play_badge.svg" alt="Google Play" width="121" height="35">
                                </a>
                            </div>
                        @endif
                    </div>
                </div>
                <div class="right--top_content_game">
                    <div class="title--top_content_game">
                        <h1>{{$post->title}}</h1>
                    </div>
                    <div class="description--top_content_game">
                        <p>{{$post->except}}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="content-single-game">
    <div class="row-delete">
        <div class="container">
            <div class="the_content">
                <p>{{$post->the_content()}}</p>
            </div>
        </div>
    </div>
</section>

@if ($post->getCountImgGallery() > 2)
<section class="slider-single-game">
    <div class="row-delete">
        <div class="container">
            <div class="box--slider-single-game">
                <div class="list-screen-game">
                    @foreach ($post->getImgGallery() as $gallery_img)
                        <a href="/uploads/{{$gallery_img}}" class="item-screen-game" rel="prettyPhoto" aria-label="Pictures">
                            <img src="/uploads/{{$gallery_img}}" alt="{{$post->title}}">
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
@endif

@isset($news)
    <section class="news-home news-single-game">
        <div class="container">
            <div class="container-news-home">
                <div class="pre-background">
                    <div class="fullbox-news">
                        <div class="wrapper--list-current-news">
                            <div class="list-items-current-news">
                                @foreach($news as $new)
                                    <a href="/news/{{$new->slug}}" class="item-current-news">
                                        <div class="picture--item-news">
                                            <img src="{{$new->getImage()}}" alt="{{$new->title}}" width="357" height="201">
                                        </div>
                                        <div class="content--item-news">
                                            <div class="title--content-news">
                                                <h4>{{$new->title}}</h4>
                                            </div>
                                            <div class="excerpt--content-news">
                                                <p>
                                                    {{$new->the_excerpt()}}
                                                </p>
                                            </div>
                                            <div class="info--content-news">
                                                <div class="data--content-news">
                                                    <span class="publish-date-news">{{$new->date}}</span>
                                                </div>
                                                <div class="detail-info-post">
                                                    <div class="time-read"><i class="fa fa-clock-o" aria-hidden="true"></i>
                                                        <span>{{$post->time}} min</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                @endforeach
                            </div>
                            <div class="read_more_news">
                                <a href="/news/">Learn more</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endisset

@include('pages.parts.footer')
