@include('pages.parts.head')

@include('pages.parts.header')

<div class="news_wrapper">
    <div class="container">
        <div class="row">
            <div class="title_game center">
                <h1>{{$tag_select->title}}</h1>
                <div class="activated-tag center">
                    <span class="green-select-button">
                        Tag
                    </span>
                </div>
            </div>
            <div class="shaddow-news">
                <section class="news-list">
                    @foreach ($posts as $post)
                        <a class="news-item" href="{{$post->the_slug()}}">
                            <div class="thumbnail-news-item">
                                <img src="{{$post->getImage()}}" alt="{{$post->title}}">
                            </div>
                            <div class="details-news-item">
                                <div class="title-details-news-item">
                                    <span>{{$post->title}}</span>
                                </div>
                                <div class="description-details-news-item">
                                    <span>{{$post->the_excerpt()}}</span>
                                </div>
                                <div class="date-details-news-item">
                                    <span>{{$post->date}}</span>
                                    <div class="detail-info-post">
                                        <div class="time-read"><i class="fa fa-clock-o" aria-hidden="true"></i>
                                            <span>{{$post->time}} min</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    @endforeach
                </section>

                @include('pages.parts.sidebar-news')

            </div>
        </div>
    </div>
</div>


@include('pages.parts.footer')
