@include('pages.parts.head')

@include('pages.parts.header')

<section class="top-banner-page">
    <div class="box-banner">
        <ul class="list-banners">
            <li class="item-banners">
                <img src="/img/office-banner.jpg" alt="Careers screen" width="1920" height="700">
            </li>
        </ul>
    </div>
</section>

<section class="wrapper page-wrapper-original">
    <div class="content-block">
        <div class="title_game">
            <h2>Careers</h2>
        </div>
        <div class="content">
            <p>Many skilled developers and other specialists are looking for a rewarding job right now. If you’re one of them, you’re lucky because we have a few options for you to take into consideration.</p>
            <p>Our team is young but we have a lot of experience in working in the gaming industry. We’re totally focused on the quality of our developments and services. We know what to do to make this industry better because we never stop learning new important things about this business.</p>
            <p>Would you like to become a part of our small but friendly team? You have such an opportunity right now! All you need to do is to explore our vacancies, study their core requirements, choose your favorable one, and apply for it. We will be glad to see young and enthusiastic people joining our company.</p>
            <p>What are the benefits of such an employment?</p>
            <ul>
                <li><p>Flexible schedule and the possibility to work from any corner of the world. We offer remote vacancies and if this is just you’re looking for, just send your CV!</p></li>
                <li><p>You will gain a lot of experience in working on big projects.</p></li>
                <li><p>You will become a part of the skilled team of developers.</p></li>
                <li><p>You will get clear feedback from your managers.</p></li>
            </ul>
        </div>
    </div>
    <div class="little-content-block">
        @if( ! empty($vacancies) )
            <div class="careers-list">
                @foreach($vacancies as $vacancy)
                    <div class="item-vacancy">
                        <div class="short-info--item-vacancy">
                            <div class="title--item-vacancy">
                                <div class="title--item-vacancy">
                                    <span>{{$vacancy->title}}</span>
                                </div>
                                <div class="hot--item-vacancy">
                                    @if ( $vacancy->is_hot === 1 )
                                    <svg width="33" height="33" viewBox="0 0 33 33" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M17.789 7.47656C18.6273 8.65028 19.078 10.0566 19.078 11.499V12.8906C19.5199 12.8906 19.949 12.7429 20.2973 12.471C20.6455 12.1991 20.8929 11.8186 21.0001 11.39L21.1405 10.8281L21.5216 11.2854C22.1359 12.0226 22.4693 12.9536 22.4627 13.9132C22.456 14.8728 22.1097 15.799 21.4852 16.5276C21.2512 16.8006 21.1288 17.1519 21.1426 17.5113C21.1563 17.8706 21.3052 18.2115 21.5595 18.4658C21.7014 18.6078 21.8717 18.7182 22.0593 18.7899C22.2468 18.8615 22.4473 18.8928 22.6478 18.8817C22.8483 18.8706 23.0441 18.8174 23.2226 18.7254C23.4011 18.6335 23.5581 18.5049 23.6835 18.3481L24.7499 17.0156L25.7812 19.5938C26.2488 21.1525 26.2856 22.8088 25.8878 24.3869C25.4899 25.9649 24.6722 27.4058 23.5215 28.5565L23.3914 28.6866C21.6201 30.4579 19.2176 31.4531 16.7126 31.4531H15.6604C14.0293 31.4531 12.426 31.0307 11.0066 30.2271C9.58719 29.4235 8.4001 28.2659 7.5609 26.8673C6.55602 25.1925 6.09504 23.2473 6.24141 21.2997C6.38779 19.352 7.13432 17.4977 8.37823 15.9919L12.5525 10.9387C12.9638 10.4408 13.2282 9.83806 13.3158 9.19819C13.4035 8.55831 13.311 7.90667 13.0488 7.31646C12.716 6.56779 12.6586 5.72558 12.8868 4.9387C13.1149 4.15182 13.614 3.47093 14.2956 3.01641L16.4999 1.54688V3.45411C16.4999 4.8965 16.9506 6.30285 17.789 7.47656Z" fill="#E82F3E"/>
                                        <path d="M20.3075 27.9347L20.312 27.9301C21.114 27.122 21.6862 26.1147 21.9697 25.012C22.2532 23.9093 22.2378 22.751 21.925 21.6562L20.425 23.1563C20.2834 23.2979 20.1153 23.4102 19.9304 23.4868C19.7454 23.5635 19.5472 23.6029 19.3469 23.6029C19.1467 23.6029 18.9485 23.5635 18.7635 23.4868C18.5786 23.4102 18.4105 23.2979 18.2689 23.1563C17.9999 22.8874 17.8411 22.5275 17.8239 22.1474C17.8066 21.7674 17.932 21.3946 18.1755 21.1023C18.677 20.5006 18.9922 19.7657 19.0825 18.9876C19.1728 18.2095 19.0343 17.422 18.684 16.7214L16.2531 11.8594L15.8793 15.9715C15.8344 16.4647 15.6905 16.9437 15.456 17.3799C15.2215 17.8161 14.9013 18.2004 14.5146 18.5098L12.3894 20.21C11.8253 20.6613 11.3699 21.2337 11.057 21.8848C10.744 22.5359 10.5815 23.2491 10.5815 23.9715V24.5566C10.5815 25.2628 10.7368 25.9603 11.0363 26.5998C11.3358 27.2393 11.7723 27.8051 12.3148 28.2572C13.193 28.9891 14.303 29.3848 15.4461 29.3735L16.9362 29.3587C18.2035 29.3461 19.4148 28.8344 20.3075 27.9347Z" fill="#EA9D2D"/>
                                        <path d="M7.73438 12.375C7.59762 12.375 7.46647 12.3207 7.36977 12.224C7.27307 12.1273 7.21875 11.9961 7.21875 11.8594V10.8281C7.21875 10.6914 7.27307 10.5602 7.36977 10.4635C7.46647 10.3668 7.59762 10.3125 7.73438 10.3125C7.87113 10.3125 8.00228 10.3668 8.09898 10.4635C8.19568 10.5602 8.25 10.6914 8.25 10.8281V11.8594C8.25 11.9961 8.19568 12.1273 8.09898 12.224C8.00228 12.3207 7.87113 12.375 7.73438 12.375Z" fill="#FF6268"/>
                                        <path d="M9.28125 9.79688C9.1445 9.79688 9.01335 9.74255 8.91665 9.64585C8.81995 9.54915 8.76562 9.418 8.76562 9.28125V8.25C8.76562 8.11325 8.81995 7.9821 8.91665 7.8854C9.01335 7.7887 9.1445 7.73438 9.28125 7.73438C9.418 7.73438 9.54915 7.7887 9.64585 7.8854C9.74255 7.9821 9.79688 8.11325 9.79688 8.25V9.28125C9.79688 9.418 9.74255 9.54915 9.64585 9.64585C9.54915 9.74255 9.418 9.79688 9.28125 9.79688Z" fill="#E82F3E"/>
                                        <path d="M7.21875 7.73438C7.082 7.73438 6.95085 7.68005 6.85415 7.58335C6.75745 7.48665 6.70312 7.3555 6.70312 7.21875V6.1875C6.70312 6.05075 6.75745 5.9196 6.85415 5.8229C6.95085 5.7262 7.082 5.67188 7.21875 5.67188C7.3555 5.67188 7.48665 5.7262 7.58335 5.8229C7.68005 5.9196 7.73438 6.05075 7.73438 6.1875V7.21875C7.73438 7.3555 7.68005 7.48665 7.58335 7.58335C7.48665 7.68005 7.3555 7.73438 7.21875 7.73438Z" fill="#961B20"/>
                                        <path d="M25.2656 13.9219C25.1289 13.9219 24.9977 13.8676 24.901 13.7709C24.8043 13.6742 24.75 13.543 24.75 13.4062V12.375C24.75 12.2382 24.8043 12.1071 24.901 12.0104C24.9977 11.9137 25.1289 11.8594 25.2656 11.8594C25.4024 11.8594 25.5335 11.9137 25.6302 12.0104C25.7269 12.1071 25.7812 12.2382 25.7812 12.375V13.4062C25.7812 13.543 25.7269 13.6742 25.6302 13.7709C25.5335 13.8676 25.4024 13.9219 25.2656 13.9219Z" fill="#E82F3E"/>
                                        <path d="M23.7188 10.3125C23.582 10.3125 23.4508 10.2582 23.3541 10.1615C23.2574 10.0648 23.2031 9.93363 23.2031 9.79688V8.76562C23.2031 8.62887 23.2574 8.49772 23.3541 8.40102C23.4508 8.30432 23.582 8.25 23.7188 8.25C23.8555 8.25 23.9867 8.30432 24.0834 8.40102C24.1801 8.49772 24.2344 8.62887 24.2344 8.76562V9.79688C24.2344 9.93363 24.1801 10.0648 24.0834 10.1615C23.9867 10.2582 23.8555 10.3125 23.7188 10.3125Z" fill="#FF6268"/>
                                        <path d="M25.7812 8.76562C25.6445 8.76562 25.5133 8.7113 25.4166 8.6146C25.3199 8.5179 25.2656 8.38675 25.2656 8.25V6.70312C25.2656 6.56637 25.3199 6.43522 25.4166 6.33852C25.5133 6.24182 25.6445 6.1875 25.7812 6.1875C25.918 6.1875 26.0492 6.24182 26.1459 6.33852C26.2426 6.43522 26.2969 6.56637 26.2969 6.70312V8.25C26.2969 8.38675 26.2426 8.5179 26.1459 8.6146C26.0492 8.7113 25.918 8.76562 25.7812 8.76562Z" fill="#E82F3E"/>
                                    </svg>
                                    @endif
                                </div>
                            </div>
                            <div class="dropdown--item-vacancy">
                                <svg width="24" height="16" viewBox="0 0 24 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M2.83003 0.584961L12 9.75493L21.17 0.584961L24 3.4149L12 15.4149L0 3.4149L2.83003 0.584961Z"/>
                                </svg>
                            </div>
                        </div>
                        <div class="details-info--item-vacancy">
                            <div class="content">
                                {{$vacancy->the_content()}}
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        @endif
        <div class="application_form">
            <div class="container-subtitle">
                <h2>SUBMIT YOUR APPLICATION</h2>
            </div>
            <div class="content-form">
                {{ Form::open([
                    'route' => 'send_resume',
                    'files' => true
                ]) }}
                <div class="original-input">
                    <label for="full_name">
                        <span class="required">Full name:</span>
                        <input id="full_name" type="text" name="name">
                    </label>
                    <label for="email">
                        <span class="required">Email:</span>
                        <input id="email" type="email" name="email">
                    </label>
                    <label for="phone">
                        <span class="required">Phone:</span>
                        <input id="phone" type="text" name="phone">
                    </label>
                    <label for="message">
                        <span>Message:</span>
                        <textarea name="message" id="message" cols="30" rows="10"></textarea>
                    </label>

                    <div class="file">
                        <span class="required">Resume:</span>
                        <div class="container-file">
                            <label for="file_resume">
                                <span class="choose_file">
                                    Choose file
                                    <input type="file" id="file_resume" name="resume" accept=".pages,.docx,.doc,.odt,.pdf">
                                </span>
                            </label>
                            <div class="loaded_file none">
                                <span id="selected_filename"></span>
                                <a href="javascript:void(0)" id="remove_file" aria-label="Remove file">
                                    <svg width="15" height="15" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M29.1742 25.1991L18.975 15L29.1742 4.80086C30.2753 3.69973 30.2753 1.92698 29.1742 0.825849C28.073 -0.275283 26.3003 -0.275283 25.1991 0.825849L15 11.025L4.80086 0.825849C3.69973 -0.275283 1.92698 -0.275283 0.825849 0.825849C-0.275283 1.92698 -0.275283 3.69973 0.825849 4.80086L11.025 15L0.825849 25.1991C-0.275283 26.3003 -0.275283 28.073 0.825849 29.1742C1.92698 30.2753 3.69973 30.2753 4.80086 29.1742L15 18.975L25.1991 29.1742C26.3003 30.2753 28.073 30.2753 29.1742 29.1742C30.2675 28.073 30.2675 26.2925 29.1742 25.1991Z" fill="#000"/>
                                    </svg>
                                </a>
                            </div>
                        </div>
                    </div>

                    <div class="submit-input">
                        <button class="btn btn-himeli">Submit application</button>
                    </div>
                    <div class="status">
                        @include('admin.errors')
                    </div>
                </div>
                {{ Form::close() }}
            </div>
        </div>
    </div>
</section>


@if ( session('status') )
    <div class="status-popup-message">
        <div class="container--status-popup-message">
            <div class="container-close-popup">
                <a id="close_popup" href="#" aria-label="close-modal">
                    <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M29.1742 25.1991L18.975 15L29.1742 4.80086C30.2753 3.69973 30.2753 1.92698 29.1742 0.825849C28.073 -0.275283 26.3003 -0.275283 25.1991 0.825849L15 11.025L4.80086 0.825849C3.69973 -0.275283 1.92698 -0.275283 0.825849 0.825849C-0.275283 1.92698 -0.275283 3.69973 0.825849 4.80086L11.025 15L0.825849 25.1991C-0.275283 26.3003 -0.275283 28.073 0.825849 29.1742C1.92698 30.2753 3.69973 30.2753 4.80086 29.1742L15 18.975L25.1991 29.1742C26.3003 30.2753 28.073 30.2753 29.1742 29.1742C30.2675 28.073 30.2675 26.2925 29.1742 25.1991Z" fill="#fff"/>
                    </svg>
                </a>
            </div>
            <div class="icon-success-popup">
                <svg width="74" height="74" viewBox="0 0 74 74" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M74 37C74 16.5633 57.4367 0 37 0C16.5633 0 0 16.5633 0 37C0 57.4367 16.5633 74 37 74C57.4367 74 74 57.4367 74 37ZM65.2848 22.5902L30.4527 57.4223L27.9668 59.9082L8.71523 40.6566L17.2137 32.1582L27.9813 42.9258L56.8008 14.1063L65.2848 22.5902Z" fill="#4FAA5A"/>
                </svg>
            </div>
            <div class="message-success-popup">
                <p class="text-success-popup">Thank you!</p>
                <p class="text-success-popup">Your request has been sent and we will definitely contact you.</p>
            </div>
        </div>
    </div>
@endif

@include('pages.parts.footer')
