@include('pages.parts.head')

@include('pages.parts.header')

<div class="news_wrapper single-news">
    <div class="container">
        <div class="row">
            <div class="shaddow-news">
                <section class="news-list">
                    <div class="title_game">
                        <h1>{{$post->title}}</h1>

                        @include('pages.parts.breadcrumbs')

                        <div class="single-line-details">
                            <div class="date-published"><span>{{$post->date}}</span></div>
                            <div class="time-read"><i class="fa fa-clock-o" aria-hidden="true"></i>
                                <span>{{$post->time}} min</span></div>
                        </div>
                    </div>
                    <div class="thumbnail-news">
                        <img src="{{$post->getImage()}}" alt="{{$post->title}}" width="837" height="471">
                    </div>
                    <div class="detail-post-news">
                        <div class="tag-container">
                            <span class="text-tag">Tags: </span>
                            <ul class="tags-list">
                            @foreach ($post->getTagListPost() as $slugTag => $titleTag)
                                <li>
                                    <a href="/tag/{{$slugTag}}">{{$titleTag}}</a>
                                </li>
                            @endforeach
                            </ul>
                        </div>
                        <div class="views-news">
                            <span class="counter-views">
                                {{$post->views}}
                            </span>
                        </div>
                    </div>
                    <div class="content">
                        {{$post->the_content()}}
                    </div>
                    <div class="share-post">
                        <a href="http://www.facebook.com/sharer.php?s=100&p[url]={{urldecode('https://himeli.org'.$post->the_slug())}}&p[title]={{$post->title}}&p[summary]={{$post->excerpt}}"
                        target="_parent" onclick="window.open(this.href, this.title, 'toolbar=0, status=0, width=548, height=325'); return false" class="share-facebook" aria-label="facebook" rel="noreferrer">
                            <img src="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMzkiIGhlaWdodD0iMzkiIHZpZXdCb3g9IjAgMCAzOSAzOSIgZmlsbD0ibm9uZSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8cGF0aCBkPSJNMjkuMjE5IDBIOS43NUM0LjM5NTI0IDAgMCA0LjM5NTI0IDAgOS43ODA5NVYyOS4yNUMwIDM0LjYwNDggNC4zOTUyNCAzOSA5Ljc1IDM5SDI5LjIxOUMzNC42MDQ4IDM5IDM5IDM0LjYwNDggMzkgMjkuMjE5VjkuNzgwOTVDMzkgNC4zOTUyNCAzNC42MDQ4IDAgMjkuMjE5IDBaTTI0LjczMSAxOS41SDIxLjA0NzZWMzEuNTcxNEgxNi40MDQ4VjE5LjVIMTMuOTI4NlYxNC41NDc2SDE2LjA5NTJWMTIuNDQyOUMxNi4wOTUyIDEwLjQ2MTkgMTcuMDg1NyA3LjMzNTcxIDIxLjI2NDMgNy4zMzU3MUgyNS4wNzE0VjExLjQ1MjRIMjIuMzc4NkMyMS45NDUyIDExLjQ1MjQgMjEuMzU3MSAxMS43MzEgMjEuMzU3MSAxMi42OTA1VjE0LjU0NzZIMjUuMTY0M0wyNC43MzEgMTkuNVoiIGZpbGw9ImJsYWNrIi8+CiAgICAgICAgICAgICAgICAgICAgICAgICAgICA8L3N2Zz4=" alt="share to facebook">
                        </a>
                        <a class="share-twitter" href="https://twitter.com/share?text={{$post->title}}&url={{urldecode('https://himeli.org'.$post->the_slug())}}" target="_parent" onclick="window.open(this.href, this.title, 'toolbar=0, status=0, width=548, height=325'); return false" aria-label="twitter" rel="noreferrer">
                            <img src="data:image/svg+xml;base64,ICAgICAgICAgICAgICAgPHN2ZyB2ZXJzaW9uPSIxLjEiIGlkPSJDYXBhXzEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHg9IjBweCIgeT0iMHB4IgogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB3aWR0aD0iMzkiIGhlaWdodD0iMzkiIHZpZXdCb3g9IjAgMCA0MzguNTM2IDQzOC41MzYiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDQzOC41MzYgNDM4LjUzNjsiCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHhtbDpzcGFjZT0icHJlc2VydmUiPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDxnPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA8cGF0aCBkPSJNNDE0LjQxLDI0LjEyM0MzOTguMzMzLDguMDQyLDM3OC45NjMsMCwzNTYuMzE1LDBIODIuMjI4QzU5LjU4LDAsNDAuMjEsOC4wNDIsMjQuMTI2LDI0LjEyMwogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgQzguMDQ1LDQwLjIwNywwLjAwMyw1OS41NzYsMC4wMDMsODIuMjI1djI3NC4wODRjMCwyMi42NDcsOC4wNDIsNDIuMDE4LDI0LjEyMyw1OC4xMDJjMTYuMDg0LDE2LjA4NCwzNS40NTQsMjQuMTI2LDU4LjEwMiwyNC4xMjYKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGgyNzQuMDg0YzIyLjY0OCwwLDQyLjAxOC04LjA0Miw1OC4wOTUtMjQuMTI2YzE2LjA4NC0xNi4wODQsMjQuMTI2LTM1LjQ1NCwyNC4xMjYtNTguMTAyVjgyLjIyNQogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgQzQzOC41MzIsNTkuNTc2LDQzMC40OSw0MC4yMDQsNDE0LjQxLDI0LjEyM3ogTTMzNS40NzEsMTY4LjczNWMwLjE5MSwxLjcxMywwLjI4OCw0LjI3OCwwLjI4OCw3LjcxCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjMCwxNS45ODktMi4zMzQsMzIuMDI1LTYuOTk1LDQ4LjEwNGMtNC42NjEsMTYuMDg3LTExLjgsMzEuNTA0LTIxLjQxNiw0Ni4yNTRjLTkuNjA2LDE0Ljc0OS0yMS4wNzQsMjcuNzkxLTM0LjM5NiwzOS4xMTUKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGMtMTMuMzI1LDExLjMyLTI5LjMxMSwyMC4zNjUtNDcuOTY4LDI3LjExN2MtMTguNjQ4LDYuNzYyLTM4LjYzNywxMC4xNDMtNTkuOTUzLDEwLjE0M2MtMzMuMTE2LDAtNjMuNzYtOC45NTItOTEuOTMxLTI2LjgzNgogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYzQuNTY4LDAuNTY4LDkuMzI5LDAuODU1LDE0LjI3NSwwLjg1NWMyNy42LDAsNTIuNDM5LTguNTY1LDc0LjUxOS0yNS43Yy0xMi45NDEtMC4xODUtMjQuNTA2LTQuMTc5LTM0LjY4OC0xMS45OTEKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGMtMTAuMTg1LTcuODAzLTE3LjI3My0xNy42OTktMjEuMjcxLTI5LjY5MWM0Ljk0NywwLjc2LDguNjU4LDEuMTM3LDExLjEzMiwxLjEzN2M0LjE4NywwLDkuMDQyLTAuNzYsMTQuNTYtMi4yNzkKICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGMtMTMuODk0LTIuNjY5LTI1LjU5OC05LjU2Mi0zNS4xMTUtMjAuNjk3Yy05LjUxOS0xMS4xMzYtMTQuMjc3LTIzLjg0LTE0LjI3Ny0zOC4xMTR2LTAuNTcxCiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjMTAuMDg1LDQuNzU1LDE5LjYwMiw3LjIyOSwyOC41NDksNy40MjJjLTE3LjMyMS0xMS42MTMtMjUuOTgxLTI4LjI2NS0yNS45ODEtNDkuOTYzYzAtMTAuNjYsMi43NTgtMjAuNzQ3LDguMjc4LTMwLjI2NAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYzE1LjAzNSwxOC40NjQsMzMuMzExLDMzLjIxMyw1NC44MTYsNDQuMjUyYzIxLjUwNywxMS4wMzgsNDQuNTQsMTcuMjI3LDY5LjA5MiwxOC41NThjLTAuOTUtMy42MTYtMS40MjctOC4xODYtMS40MjctMTMuNzA0CiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjMC0xNi41NjIsNS44NTMtMzAuNjkyLDE3LjU2LTQyLjM5OWMxMS43MDMtMTEuNzA2LDI1LjgzNy0xNy41NjEsNDIuMzk0LTE3LjU2MWMxNy41MTUsMCwzMi4wNzksNi4yODMsNDMuNjg4LDE4Ljg0NgogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgYzEzLjEzNC0yLjQ3NCwyNS44OTItNy4zMywzOC4yNi0xNC41NmMtNC43NTcsMTQuNjUyLTEzLjYxMywyNS43ODgtMjYuNTUsMzMuNDAyYzEyLjM2OC0xLjcxNiwyMy44OC00Ljk1LDM0LjUzNy05LjcwOAogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgQzM1Ny40NTgsMTQ5Ljc5MywzNDcuNDYyLDE2MC4xNjYsMzM1LjQ3MSwxNjguNzM1eiIvPgogICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvZz4KICAgICAgICAgICAgICAgICAgICAgICAgICAgIDwvc3ZnPg==" alt="share to Twitter">
                        </a>
                    </div>
                    @isset ($targetPost)
                    <div class="releated-post">
                        <a class="news-item" href="{{$targetPost->the_slug()}}">
                            <div class="thumbnail-news-item">
                                <img src="{{$targetPost->getImage()}}" alt="{{$targetPost->title}}" width="280" height="205">
                            </div>
                            <div class="details-news-item">
                                <div class="title-details-news-item">
                                    <span>{{$targetPost->title}}</span>
                                </div>
                                <div class="description-details-news-item">
                                    <span>{{$targetPost->the_excerpt()}}</span>
                                </div>
                                <div class="date-details-news-item">
                                    <span>{{$targetPost->date}}</span>
                                </div>
                            </div>
                        </a>
                    </div>
                    @endisset
                </section>

                @include('pages.parts.sidebar-news')

            </div>
        </div>
    </div>
</div>


@include('pages.parts.footer')
