@include('pages.parts.head')

@include('pages.parts.header')

<h1 style="display: none;">HiMeli</h1>

<section class="top-banner-page">
    <div class="box-banner">
        <ul class="list-banners">
            <li class="item-banners">
                <img src="/img/main_banner_3.png" alt="we'll implement your ideas" width="1920" height="700">
            </li>
        </ul>
    </div>
</section>

@isset($top_game)
    <section class="top-game-banner">
        <div class="content-block">
            <div class="shadow-container">
                <div class="container_top_game">
                    <div class="screenshot_top_game">
                        <img src="/uploads/{{$top_game->banner_game}}" alt="{{$top_game->title}}" width="655" height="320">
                    </div>
                    <div class="information_top_game">
                        <div class="title--information_top_game">
                            <h5>{{$top_game->title}}</h5>
                        </div>
                        <div class="description--information_top_game">
                            <p>{{$top_game->except}}</p>
                        </div>
                        <div class="links--information_top_game">
                            <div class="read_more_top_game">
                                <a href="/games/{{$top_game->slug}}">Learn more</a>
                            </div>
                            @if ($top_game->app_store_link)
                                <div class="app_store_top_game">
                                    <a href="{{$top_game->app_store_link}}" target="_blank" aria-label="App Store" rel="noreferrer">
                                        <img src="/img/app_store_badge.svg" alt="App Store" width="121" height="36">
                                    </a>
                                </div>
                            @endif
                            @if ($top_game->play_market_link)
                                <div class="google_play_top_game">
                                    <a href="{{$top_game->play_market_link}}" target="_blank" aria-label="Google Play" rel="noreferrer">
                                        <img src="/img/google_play_badge.svg" alt="Google Play" width="121" height="36">
                                    </a>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endisset

@if ($latest_game->count() > 2)
<section class="feature-games-slider">
    <div class="container">
        <div class="title_game">
            <h2>Latest Games</h2>
        </div>
        <div class="container-slider-feature-games-slider">
            <div class="slider-list-wrapper-feature-slider">
                @foreach ($latest_game as $late_game)
                    <span class="item-feature-slider">
                        <div style="background-image: url(/uploads/{{$late_game->thumbnail_game}})" title="{{$late_game->title}}">
                            <div class="item-menu-feature-slider">
                                <div class="item-menu-current">
                                    <a href="/games/{{$late_game->slug}}">More</a>
                                </div>
                                <div class="item-menu-current">
                                    <a href="#">Blog</a>
                                </div>
                                @isset($late_game->forum_link)
                                    <div class="item-menu-current">
                                    <a href="{{$late_game->forum_link}}">Forum</a>
                                </div>
                                @endisset
                            </div>
                        </div>
                    </span>
                @endforeach
            </div>
        </div>
    </div>
</section>
@endif

@if($posts)
    <section class="news-home">
        <div class="container">
            <div class="title_game">
                <h2>News</h2>
            </div>
            <div class="container-news-home">
                <div class="pre-background">
                    <div class="fullbox-news">
                        <div class="wrapper--list-current-news">
                            <div class="list-items-current-news">
                                @foreach($posts as $post)
                                    <a href="/news/{{$post->slug}}" class="item-current-news">
                                        <div class="picture--item-news">
                                            <img src="{{$post->getImage()}}" alt="{{$post->title}}" width="357" height="201">
                                        </div>
                                        <div class="content--item-news">
                                            <div class="title--content-news">
                                                <h4>{{$post->title}}</h4>
                                            </div>
                                            <div class="excerpt--content-news">
                                                <p>
                                                    {{$post->the_excerpt()}}
                                                </p>
                                            </div>
                                            <div class="info--content-news">
                                                <div class="data--content-news">
                                                    <span class="publish-date-news">{{$post->date}}</span>
                                                </div>
                                                <div class="detail-info-post">
                                                    <div class="time-read"><i class="fa fa-clock-o" aria-hidden="true"></i>
                                                        <span>{{$post->time}} min</span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                @endforeach
                            </div>
                            <div class="read_more_news">
                                <a href="/news/">Learn more</a>
                            </div>
                        </div>

                        <div class="wrapper--list-categories-news">
                            <div class="container-list-tabs">
                                <ul class="box--list-tabs" data-token="{{ csrf_token() }}">
                                    @if ( $categories->count() > 0 )
                                        @foreach ($categories as $category)
                                            <li class="item-tabs">
                                                <span class="box--item-tabs {{$current = $category->title === 'News' ? 'current' : '' }}">
                                                    <a class="item-tabs--link" href="javascript:void(0)" data-source-cat="{{$category->slug}}">{{$category->title}}</a>
                                                </span>
                                            </li>
                                        @endforeach
                                    @endif
                                </ul>
                            </div>
                        </div>

                        <div class="wrapper--list-categories-news-mobile">
                            <div class="categories-list">
                                @if ( $categories->count() > 0 )
                                    <div class="title-categories-form">
                                        <h3>Category</h3>
                                    </div>
                                    <ul class="box--list-tabs">
                                        @foreach ($categories as $category)
                                            <li class="item-tabs">
                                            <span class="box--item-tabs {{$current = $category->title === 'News' ? 'current' : '' }}">
                                                <a class="item-tabs--link" href="javascript:void(0)" data-source-cat="{{$category->slug}}">{{$category->title}}</a>
                                            </span>
                                            </li>
                                        @endforeach
                                    </ul>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endif

@include('pages.parts.footer')
