<div class="site">
    <header>
        <div class="box-header">
            <div class="main-header">
                <nav class="navbar main-menu navbar-expand-lg">
                    <div class="menu-content">
                        <!-- Brand and toggle get grouped for better mobile display -->
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="{{route('home')}}"><img src="/img/logoS_HiMeli_2020-09.svg" alt="HiMeli" width="60" height="60"></a>
                        </div> <!-- end navbar-header -->
                        <div class="collapse navbar-collapse " id="bs-example-navbar-collapse-1">
                            <ul class="nav navbar-nav">
                                <li><a href="{{route('all_games')}}">Games</a></li>
                                <li><a href="{{route('all_news')}}">News</a></li>
                                <li><a href="{{route('about_us')}}">About Us</a></li>
                                <li><a href="{{route('careers')}}">Careers</a></li>
                            </ul>
                            @if(Auth::check())
                                <ul class="dropdown-container-main-menu pull-right">
                                    <li class="profile-item">
                                        <img class="avatar-profile" src="{{Auth::user()->getAvatar()}}" alt="">
                                        <div class="nav navbar-wrappper">
                                            <ul class="navbar-list">
                                                <li><a href="/admin/">Admin Panel</a></li>
                                                <li><a href="/profile/">My profile</a></li>
                                                <li><a href="/logout/">Logout</a></li>
                                            </ul>
                                        </div>
                                    </li>
                                </ul>
                            @endif
                        </div><!-- /.navbar-collapse -->
                    </div>
                    <!-- /.container-fluid -->
                </nav>
                <div class="search-form">
                    <div class="form-box">
                        <div class="form-container-popup">
                            <div class="close-search-form">
                                <a href="javascript:void(0)" class="btn--close-search-form">X</a>
                            </div>
                            <form action="">
                                <input type="text" placeholder="Search" name="search">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <div class="wrapper-content">
