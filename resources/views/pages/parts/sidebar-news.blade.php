<aside class="news-sidebar">

    @if (session('status'))
        <div class="alert alert-success">
            {{session('status')}}
        </div>
    @endif
    <div class="subscribe-form">
        <div class="title-subscribe-form">
            <h3>More news</h3>
        </div>
        <div class="form-subscribe-form">
            <form action="/subscribe" method="post">
                {{csrf_field()}}
                <div class="label-input-subscribe-email">
                    <input type="text" class="input-subscribe-email" name="email" placeholder="Your email address">
                    @include('admin.errors')
                </div>
                <div class="submit-input-subscribe-email">
                    <button class="btn btn-himeli">Subscribe</button>
                </div>
            </form>
        </div>
    </div>

    <div class="categories-list">
        <div class="title-categories-form">
            <h3>Categories</h3>
        </div>
        <div class="list-categories-form">
            <a class="activated-tag" href="/news/"><span class="green-select-button">All</span></a>
            @foreach ($categories as $category)
                @if ($category->posts->count() > 0 )
                    <a class="activated-tag" href="/category/{{$category->slug}}"><span class="green-select-button">{{$category->title}}</span></a>
                @endif
            @endforeach
        </div>
    </div>

    <div class="categories-list">
        <div class="title-categories-form">
            <h3>Tags</h3>
        </div>
        <div class="list-categories-form">
            <a class="activated-tag" href="/news/"><span class="green-select-button">All</span></a>
            @foreach ($tags as $tag)
                @if($tag->posts->count() > 0)
                    <a class="activated-tag" href="/tag/{{$tag->slug}}"><span class="green-select-button">{{$tag->title}}</span></a>
                @endif
            @endforeach
        </div>
    </div>

</aside>