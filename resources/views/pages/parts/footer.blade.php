    <footer>
        <div class="background-footer">
            <div class="footer-logo">
                <a class="himeli-brand" href="/"><img src="/img/logoB_HiMeli_2020-10.svg" alt="HiMeli" width="129" height="56" aria-label="HiMeli"></a>
            </div>
            <div class="list-technical-page">
                <a href="javascript:void(0)">Cookies</a>
                <a href="{{route('privacy_policy')}}">Privacy</a>
                <a href="javascript:void(0)">Legal</a>
                <a href="javascript:void(0)">Terms</a>
            </div>
            <div class="list-socials">
                <div class="item-social">
                    <a href="https://www.facebook.com/himeliofficial/" target="_blank" aria-label="facebook" rel="noreferrer">
                        <i class="fa fa-facebook-official" aria-hidden="true"></i>
                    </a>
                    <a href="https://www.instagram.com/himeliofficial/" target="_blank" aria-label="instagram" rel="noreferrer">
                        <i class="fa fa-instagram" aria-hidden="true"></i>
                    </a>
                    <a href="https://www.youtube.com/channel/UCRnPEtgvpJqacaaWmvb6WpA" target="_blank" aria-label="youtube" rel="noreferrer">
                        <i class="fa fa-youtube-play" aria-hidden="true"></i>
                    </a>
                    <a href="https://twitter.com/himeliofficial" target="_blank" aria-label="twitter" rel="noreferrer">
                        <i class="fa fa-twitter" aria-hidden="true"></i>
                    </a>
                </div>
            </div>
            <div class="copy">
                <p>© {{date('Y')}} HiMeli. All rights reserved.</p>
            </div>
        </div>
    </footer>

</div> <!-- end wrapper-content --->
    @if ( Cookie::get('himeli-cookie-agree') === null )
        <div class="cookies" data-token-cookies="{{ csrf_token() }}">
            <div class="cookies-wrapper">
                <div class="info--cookies-wrapper">
                    <p class="title--cookies-wrapper">Notice</p>
                    <p class="text--cookies-wrapper">Our website uses cookies to personalise content and to analyse our traffic. Check our
                        <a href="{{route('privacy_policy')}}">privacy_policy</a> and cookie policy to learn more on how we process your personal data.</p>
                    <p class="text--cookies-wrapper">By pressing Accept you agree with these terms.</p>
                </div>

                <div class="btns--cookies-wrapper">
                    <a href="javascript:void(0)" class="reject-cookies btn-cancel">Decline</a>
                    <a href="javascript:void(0)" class="accept-cookies btn-himeli">Accept</a>
                </div>
            </div>
        </div>
    @endif
</div> <!-- end site --->

<!-- JS Files -->
<script defer src="/js/front.js"></script>

</body>
</html>
