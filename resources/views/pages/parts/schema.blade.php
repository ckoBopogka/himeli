<!-- Schemas -->
@isset( $organization )
    {!! $organization !!}
@endisset

@isset( $website )
    {!! $website !!}
@endisset

@isset( $breadcrumbs )
    {!! $breadcrumbs !!}
@endisset

@isset( $article )
    {!! $article !!}
@endisset

@isset( $person )
    {!! $person !!}
@endisset
