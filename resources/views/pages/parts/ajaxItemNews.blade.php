@isset($posts)
    @foreach($posts as $post)
        <a href="/news/{{$post->slug}}" class="item-current-news">
            <div class="picture--item-news">
                <img src="{{$post->getImage()}}" alt="{{$post->title}}">
            </div>
            <div class="content--item-news">
                <div class="title--content-news">
                    <h4>{{$post->title}}</h4>
                </div>
                <div class="excerpt--content-news">
                    <p>
                        {{$post->the_excerpt()}}
                    </p>
                </div>
                <div class="info--content-news">
                    <div class="data--content-news">
                        <span class="publish-date-news">{{$post->date}}</span>
                    </div>
                </div>
            </div>
        </a>
    @endforeach
@endisset
