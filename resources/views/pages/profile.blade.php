@extends('admin.layout')

@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Профиль</h1>
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="box">
                <div class="box-body">
                    <div class="box--body-header">
                        @if(session('status'))
                            <div class="alert alert-success">
                                {{session('status')}}
                            </div>
                        @endif
                        @include('admin.errors')
                    </div>
                    <div class="box--body-content">
                        <div class="avatar-content">
                            <img src="{{$user->getAvatar()}}" alt="" class="profile-image">
                        </div>
                        <div class="form-profile">
                            <h4>Информация об профиле: </h4>
                            <form class="form-horizontal contact-form" role="form" method="post" action="/profile" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="name">
                                            <p>Имя: </p>
                                            <input type="text" class="form-control" id="name" name="name" value="{{$user->name}}">
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="email">
                                            <p>Email: </p>
                                            <input type="email" class="form-control" id="email" name="email" value="{{$user->email}}">
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="password">
                                            <p>Пароль: </p>
                                            <input type="password" class="form-control" id="password" name="password">
                                        </label>
                                    </div>
                                </div>

{{--                                <div class="form-group">--}}
{{--                                    <div class="col-md-12">--}}
{{--                                        <label for="position">--}}
{{--                                            <p>Должность: </p>--}}
{{--                                            <input type="text" class="form-control" id="position" name="position">--}}
{{--                                        </label>--}}
{{--                                    </div>--}}
{{--                                </div>--}}

{{--                                <div class="form-group">--}}
{{--                                    <div class="col-md-12">--}}
{{--                                        <label for="role">--}}
{{--                                            <p>Роль: </p>--}}
{{--                                            <input type="text" class="form-control" id="role" name="role">--}}
{{--                                        </label>--}}
{{--                                    </div>--}}
{{--                                </div>--}}

                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label for="avatar">
                                            <p>Изображение профиля: </p>
                                            <input type="file" class="form-control" id="avatar" name="avatar">
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-12">
                                        <label class="notflex" for="description">
                                            <p>Информация об профиле: </p>
                                            <textarea name="description" id="description"></textarea>
                                        </label>
                                    </div>
                                </div>

                                <button type="submit" class="btn send-btn">Обновить</button>
                            </form>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">

                </div>
                <!-- /.box-footer-->
            </div>

            <div class="container">
                <div class="row">

                </div>
            </div>
            <!-- /.box -->

        </section>
        <!-- /.content -->
    </div>
@endsection
