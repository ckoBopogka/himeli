@extends('admin.layout')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Добро пожаловать
            <small>{{Auth::user()->name}}</small>
        </h1>
    </section>

    <!-- Main content -->
    <section class="content">
        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Главная страница</h3>
            </div>
            <div class="box-body">
                <p>В Админ-панели есть администрация и редактора, которые занимаются заполнением сайта
                из данной панели.</p>
                <br>Если вы наблюдаете или возникают какие-то проблемы с работой Админ-панели,</br>
                то пожалуйста предоставьте главному админу скриншоты и ссылку, где обнаружили проблему.</p>
                <p>Сайт и Админ-панель находится в постоянной разработке.</p>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <p>Контактная информация администратора:</p>
                <ul>
                    <li>E-mail: <a href="mailTo:polkovnik_denis@live.com">polkovnik_denis@live.com</a></li>
                    <li>Telegram: @polkovnik_denis</li>
                    <li>Discord: <a href="https://discord.gg/rzdX9bsQ">HiMeli Server</a></li>
                </ul>
            </div>
            <!-- /.box-footer-->
        </div>

        <div class="container">
            <div class="row">

            </div>
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->
</div>
@endsection