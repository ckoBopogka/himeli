@extends('admin.layout')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Изменить тег "{{$tag->title}}"</h1>
        </section>

        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            {{Form::open([
	            'route'=>['tags.update', $tag->id],
	            'method'=>'put'
	        ])}}
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Меняем тег</h3>
                    @include('admin.errors')
                </div>
                <div class="box-body">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="slug">Ссылка</label>
                            <a href="https://himeli.org/tag/{{$tag->slug}}/" target="_blank">https://himeli.org/tag/{{$tag->slug}}/</a>
                            <input type="text" class="form-control" id="slug" name="slug" value="{{$tag->slug}}">
                        </div>
                        <div class="form-group">
                            <label for="title_tag">Название</label>
                            <input type="text" class="form-control" name="title" id="title_tag" value="{{$tag->title}}">
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button class="btn btn-default">Назад</button>
                    {{Form::close()}}
                    <button class="btn btn-warning pull-right">Изменить</button>
                </div>
                <!-- /.box-footer-->
            </div>
            <!-- /.box -->

        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
