@extends('admin.layout')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Вакансии</h1>
            <ol class="breadcrumb">
                <li><a href="/admin/"><i class="fa fa-dashboard"></i> Админ-панель</a></li>
                <li class="active">Вакансии</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Default box -->
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <a href="{{route('vacancies.create')}}" class="btn btn-success">Добавить вакансию</a>
                    </div>
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Название</th>
                            <th>Действия</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($vacancies as $vacancy)
                                <tr>
                                    <td>{{$vacancy->id}}</td>
                                    <td>{{$vacancy->title}}@if($vacancy->status != 0) {{" - Черновик"}}@endif</td>
                                    <td>
                                        <a href="{{route('vacancies.edit', $vacancy->id)}}" class="fa fa-pencil"></a>

                                        {{Form::open( ['route' => ['vacancies.destroy', $vacancy->id], 'method'=>'delete'] )}}
                                            <button onclick="return confirm('Вы уверены?')" type="submit" class="delete"><i class="fa fa-remove"></i></button>
                                        {{Form::close()}}
                                    </td>
                                </tr>
                            @endforeach
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
