@extends('admin.layout')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper create-post">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Добавить вакансию</h1>
        </section>

        <!-- Main content -->
        <section class="content">
            {{ Form::open([
                'route' => 'vacancies.store',
                'files' => true
            ]) }}
            <!-- Default box -->

            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"></h3>
                    @include('admin.errors')
                </div>
                <div class="box-body">
                    <div class="col-md-9">
                        <div class="form-group">
                            <label for="title">Название<span class="required">*</span></label>
                            <input type="text" class="form-control" id="title" placeholder="" name="title" value="{{ old('title') }}">
                        </div>
                        <div class="form-group">
                            <label for="content">Полный текст<span class="required">*</span></label>
                            <textarea name="content" id="content" cols="50" rows="30" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Дата релиза/последнего обновления<span class="required">*</span></label>
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right" id="datepicker" name="date" value="{{ old('date') }}" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group">
                            <label>
                                <input type="checkbox" class="minimal" name="is_hot">
                            </label>
                            <label>Горячая вакансия</label>
                        </div>
                        <div class="form-group">
                            <label>
                                <input type="checkbox" class="minimal" name="status">
                            </label>
                            <label>Черновик</label>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button class="btn btn-default">Назад</button>
                    <button class="btn btn-success pull-right">Добавить</button>
                </div>
                <!-- /.box-footer-->
            </div>
            <!-- /.box -->
            {{ Form::close() }}
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
