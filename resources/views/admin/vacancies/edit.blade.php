@extends('admin.layout')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Изменить вакансию <strong>{{ $vacancy->title }}</strong></h1>
        </section>

        <!-- Main content -->
        <section class="content">
            {{ Form::open([
                'route' => ['vacancies.update', $vacancy->id],
                'files' => true,
                'method'=> 'put'
            ]) }}
            <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    @include('admin.errors')
                </div>
                <div class="box-body">
                    <div class="col-md-9">
                        <div class="form-group">
                            <label for="title">Название<span class="required">*</span></label>
                            <input type="text" class="form-control" id="title" name="title" value="{{ $vacancy->title }}">
                        </div>

                        <div class="form-group">
                            <label for="content">Полный текст<span class="required">*</span></label>
                            <textarea name="content" id="content" cols="50" rows="30" class="form-control">{{ $vacancy->content }}</textarea>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Дата обновления<span class="required">*</span></label>
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right" id="datepicker" name="date" value="{{ $vacancy->date }}" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group">
                            <label>
                                {{ Form::checkbox('is_hot', '1', $vacancy->is_hot, ['class' => 'minimal']) }}
                            </label>
                            <label>Горячая вакансия</label>
                        </div>
                        <!-- checkbox -->
                        <div class="form-group">
                            <label>
                                {{ Form::checkbox('status', '1', $vacancy->status, ['class' => 'minimal']) }}
                            </label>
                            <label>Черновик</label>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button class="btn btn-default">Назад</button>
                    <button class="btn btn-warning pull-right">Изменить</button>
                </div>
                <!-- /.box-footer-->
            </div>
            <!-- /.box -->
            {{ Form::close() }}
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
