@extends('admin.layout')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

        @if (session('status'))
            <div class="alert alert-success">
                {{session('status')}}
            </div>
        @endif

        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Основные настройки сайта</h1>
        </section>

        <!-- Main content -->
        <section class="content">
        {{Form::open(['route' => 'settings.store', 'files'	=>	true])}}
        <!-- Default box -->
            @include('admin.errors')

            <div class="box">
                <div class="box-header with-border">
                    <ul class="tabs-list">
                        <li class="tab-bar-item general active">
                            <a data-tab="general" href="javascript:void(0);">Основные настройки</a>
                        </li>
                        <li class="tab-bar-item smtp">
                            <a data-tab="smtp" href="javascript:void(0);">Настройки SMTP</a>
                        </li>
                        <li class="tab-bar-item seo">
                            <a data-tab="seo" href="javascript:void(0);">Настройки SEO</a>
                        </li>
                        <li class="tab-bar-item widgets">
                            <a data-tab="widgets" href="javascript:void(0);">Виджеты и роли</a>
                        </li>
                        <li class="tab-bar-item posts">
                            <a data-tab="posts" href="javascript:void(0);">Выводы постов</a>
                        </li>
                    </ul>
                </div>
                <div class="box-body tabs-body">
                    <div id="general" class="tab-item active">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="site_name">Название сайта</label>
                                <input type="text" name="options_name[site_name]" class="form-control" id="site_name" placeholder="" value="@isset($options[0]) {{$options[0]->options_value}} @endisset">
                            </div>
                            <div class="form-group">
                                <label for="site_description">Описание сайта</label>
                                <input type="text" name="options_name[site_description]" class="form-control" id="site_description" placeholder="" value="@isset($options[1]) {{$options[1]->options_value}} @endisset">
                            </div>
                        </div>
                    </div>
                    <div id="smtp" class="tab-item">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="site_smtp_host">Почтовый сервер(хост)</label>
                                <input type="text" name="options_name[site_smtp_host]" class="form-control" id="site_smtp_host" placeholder="" value="@isset($options[2]) {{$options[2]->options_value}} @endisset">
                            </div>
                            <div class="form-group">
                                <label for="site_smtp_port">Порт сервера</label>
                                <input type="text" name="options_name[site_smtp_port]" class="form-control" id="site_smtp_port" placeholder="" value="@isset($options[3]) {{$options[3]->options_value}} @endisset">
                            </div>
                            <div class="form-group">
                                <label for="site_smtp_user_name">Имя пользователя(E-mail)</label>
                                <input type="text" name="options_name[site_smtp_user_name]" class="form-control" id="site_smtp_user_name" placeholder="" value="@isset($options[4]) {{$options[4]->options_value}} @endisset">
                            </div>
                            <div class="form-group">
                                <label for="site_smtp_user_password">Пароль пользователя</label>
                                <input type="password" name="options_name[site_smtp_user_password]" class="form-control" id="site_smtp_user_password" placeholder="" value="@isset($options[5]) {{$options[5]->options_value}} @endisset">
                            </div>
                        </div>
                    </div>
                    <div id="seo" class="tab-item">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="site_seo_pictures_default">Изображение для соц.сетей (по умолчанию)</label>
                                <input type="file" name="options_name[site_seo_pictures_default]" id="site_seo_pictures_default" value="@isset($options[6]) {{$options[6]->options_value}} @endisset">
                            </div>
                        </div>
                    </div>
                    <div id="widgets" class="tab-item">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="widgets_options">Вывод виджетов на главную админ панели</label>
                            </div>
                        </div>
                    </div>
                    <div id="posts" class="tab-item">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="posts_options">Вывод постов</label>
{{--                                <input type="number" name="options_name[posts_options]" class="form-control" id="posts_options" placeholder="">--}}
                            </div>
                        </div>
                    </div>
                </div>

                <!-- /.box-body -->
                <div class="box-footer">
                    <button class="btn btn-success pull-right">Добавить</button>
                </div>
                <!-- /.box-footer-->
            </div>
            <!-- /.box -->
            {{Form::close()}}
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
