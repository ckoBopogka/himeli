@extends('admin.layout')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Изменить игру <strong>{{$game->title}}</strong>
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            {{Form::open([
                'route' => ['posts-game.update', $game->id],
                'files' => true,
                'method'=> 'put'
            ])}}
            <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    @include('admin.errors')
                </div>
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="slug">Ссылка</label>
                            <a href="https://himeli.org/games/{{$game->slug}}/" target="_blank">https://himeli.org/games/{{$game->slug}}/</a>
                            <input type="text" class="form-control" id="slug" name="slug" value="{{$game->slug}}" style="max-width: 320px">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Название<span class="required">*</span></label>
                            <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="title" value="{{$game->title}}" autocomplete="off">
                        </div>

                        <div class="form-group">
                            <label>Ссылка на Play Market</label>
                            <input type="url" class="form-control" id="exampleInputEmail1" placeholder="" name="play_market_link" value="{{$game->play_market_link}}" autocomplete="off">
                        </div>

                        <div class="form-group">
                            <label>Ссылка на App Store</label>
                            <input type="url" class="form-control" id="exampleInputEmail1" placeholder="" name="app_store_link" value="{{$game->app_store_link}}" autocomplete="off">
                        </div>

                        <div class="form-group">
                            <label>Ссылка на тему форума</label>
                            <input disabled type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="forum_link" value="{{$game->forum_link}}" autocomplete="off">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="keywords">Ключевые слова<span class="required">*</span></label>
                            <input type="text" class="form-control" id="keywords" name="keywords" value="{{ $game->keywords }}">
                        </div>
                        <div class="form-group">
                            <label for="except">Короткий текст:<span class="required">*</span></label>
                            <textarea name="except" id="except" cols="30" rows="5" class="form-control">{{$game->except}}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="content">Полный текст:<span class="required">*</span></label>
                            <textarea name="content" id="content" cols="50" rows="30" class="form-control">{{$game->content}}</textarea>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Дата релиза/последнего обновления:<span class="required">*</span></label>

                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right" id="datepicker" name="date" value="{{$game->date}}" autocomplete="off">
                            </div>
                        </div>

                        <!-- checkbox -->
                        <div class="form-group">
                            <label>
                                {{Form::checkbox('is_top', '0', $game->is_top, ['class' => 'minimal'])}}
                            </label>
                            <label>
                                Выводить в ТОП
                            </label>
                        </div>
                        <!-- checkbox -->
                        <div class="form-group">
                            <label>
                                {{Form::checkbox('is_popular', '0', $game->is_popular, ['class' => 'minimal'])}}
                            </label>
                            <label>
                                Выводить в популярные
                            </label>
                        </div>
                        <div class="form-group">
                            <label>
                                {{Form::checkbox('is_coming_soon', '0', $game->is_coming_soon, ['class' => 'minimal'])}}
                            </label>
                            <label>
                                В разработке
                            </label>
                        </div>
                        <!-- checkbox -->
                        <div class="form-group">
                            <label>
                                {{Form::checkbox('is_featured', '0', $game->is_featured, ['class' => 'minimal'])}}
                            </label>
                            <label>
                                Рекомендовать
                            </label>
                        </div>
                        <!-- checkbox -->
                        <div class="form-group">
                            <label>
                                {{Form::checkbox('status', '1', $game->status, ['class' => 'minimal'])}}
                            </label>
                            <label>
                                Черновик
                            </label>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="label-from-group">
                                    <label>Баннер игры<span class="required">*</span></label>
                                    <div class="helpers">
                                        <span class="help">?</span>
                                        <span class="description_help">Загружайте изображение по размерам: 1920x700</span>
                                    </div>
                                </div>
                                <img src="{{$game->getBanner()}}" alt="" class="img-responsive" width="200">
                                <input type="file" name="banner_game" id="banner_game" class="inputfiles" data-multiple-caption="{count} files selected" multiple />
                                <label for="banner_game" class="labelinputfiles" style="margin-top: 15px;">Выбрать баннера</label>
                                <span class="file_names"></span>
                                <p class="help-block">jpg, jpeg, png</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="label-from-group">
                                    <label>Миниатюра игры<span class="required">*</span></label>
                                    <div class="helpers">
                                        <span class="help">?</span>
                                        <span class="description_help">Загружайте изображение по размерам: 512x512</span>
                                    </div>
                                </div>
                                <img src="{{$game->getImage()}}" alt="" class="img-responsive" width="200">
                                <input type="file" name="thumbnail_game" id="thumbnail_game" class="inputfiles" data-multiple-caption="{count} files selected" multiple />
                                <label for="thumbnail_game" class="labelinputfiles" style="margin-top: 15px;">Выбрать иконку</label>
                                <span class="file_names"></span>
                                <p class="help-block">jpg, jpeg, png</p>
                            </div>
                        </div>

                        <!-- Gallery game -->
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="label-from-group">
                                    <label>Изображения/скриншоты</label>
                                    <div class="helpers">
                                        <span class="help">?</span>
                                        <span class="description_help">Загружайте изображение по размерам: 700x600</span>
                                    </div>
                                </div>
                                <input type="file" name="files_gallery[]" id="files_gallery" class="inputfiles" data-multiple-caption="{count} files selected" multiple />
                                <label for="files_gallery" class="labelinputfiles">Выбрать файлы</label>
                                <span class="file_names"></span>
                                <p>{{$game->getCountImgGallery()}} files</p>
                                <p class="help-block">jpg, jpeg, png</p>
                            </div>
                        </div>

                    </div>
                </div>

                <!-- /.box-body -->
                <div class="box-footer">
                    <button class="btn btn-default">Назад</button>
                    <button class="btn btn-warning pull-right">Изменить</button>
                </div>
                <!-- /.box-footer-->
            </div>
            <!-- /.box -->
            {{Form::close()}}
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection