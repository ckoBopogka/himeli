@extends('admin.layout')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Игры</h1>
            <ol class="breadcrumb">
                <li><a href="/admin/"><i class="fa fa-dashboard"></i> Админ-панель</a></li>
                <li class="active">Игры</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Default box -->
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"></h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <a href="{{route('posts-game.create')}}" class="btn btn-success">Добавить игру</a>
                    </div>
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Название</th>
                            <th>Картинка</th>
                            <th>Действия</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($games as $game)
                                <tr>
                                    <td>{{$game->id}}</td>
                                    <td>
                                        <a href="/games/{{$game->slug}}" target="_blank">{{$game->title}}</a>
                                        @if($game->status != 0)
                                            {{" - Черновик"}}
                                        @endif
                                    </td>
                                    <td>
                                        <img src="{{$game->getImage()}}" alt="" width="100">
                                    </td>
                                    <td>
                                        <a href="{{route('posts-game.edit', $game->id)}}" class="fa fa-pencil"></a>
                                        {{Form::open( ['route' => ['posts-game.destroy', $game->id], 'method'=>'delete'] )}}
                                            <button onclick="return confirm('Вы уверены?')" type="submit" class="delete"><i class="fa fa-remove"></i></button>
                                        {{Form::close()}}
                                    </td>
                                </tr>
                            @endforeach
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection