@extends('admin.layout')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper create-game">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Добавить новую игру
            </h1>
        </section>

        <!-- Main content -->
        <section class="content">
            {{Form::open([
                'route' => 'posts-game.store',
                'files' => true
            ])}}
            <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Добавляем игру</h3>
                    @include('admin.errors')
                </div>
                <div class="box-body">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="title">Название<span class="required">*</span></label>
                            <input type="text" class="form-control" id="title" name="title" value="{{old('title')}}">
                        </div>

                        <div class="form-group">
                            <label>Ссылка на Play Market</label>
                            <input type="url" class="form-control" id="play_market_link" name="play_market_link" value="{{old('play_market_link')}}" autocomplete="off">
                        </div>

                        <div class="form-group">
                            <label>Ссылка на App Store</label>
                            <input type="url" class="form-control" id="app_store_link" name="app_store_link" value="{{old('app_store_link')}}" autocomplete="off">
                        </div>

                        <div class="form-group">
                            <label>Ссылка на тему форума</label>
                            <input disabled type="text" class="form-control" id="forum_link" name="forum_link" value="{{old('forum_link')}}" autocomplete="off">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="keywords">Ключевые слова<span class="required">*</span></label>
                            <input type="text" class="form-control" id="keywords" placeholder="" name="keywords" value="{{ old('keywords') }}">
                        </div>
                        <div class="form-group">
                            <label for="except">Короткий текст:<span class="required">*</span></label>
                            <textarea name="except" id="except" cols="30" rows="5" class="form-control"></textarea>
                        </div>

                        <div class="form-group">
                            <label for="content">Полный текст:<span class="required">*</span></label>
                            <textarea name="content" id="content" cols="50" rows="30" class="form-control"></textarea>
                        </div>
                    </div>

                    <div class="col-md-3 sidebar-publication-default">
                        <div class="form-group">
                            <label>Дата релиза/последнего обновления:<span class="required">*</span></label>

                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right" id="datepicker" name="date" value="{{old('date')}}" autocomplete="off">
                            </div>
                            <!-- /.input group -->
                        </div>

                        <!-- checkbox -->
                        <div class="form-group">
                            <label>
                                <input type="checkbox" class="minimal" name="is_top">
                            </label>
                            <label class="sidebar-checkbox-label">
                                Выводить в ТОП
                            </label>
                        </div>

                        <!-- checkbox -->
                        <div class="form-group">
                            <label>
                                <input type="checkbox" class="minimal" name="is_popular">
                            </label>
                            <label class="sidebar-checkbox-label">
                                Выводить в популярные
                            </label>
                        </div>

                        <!-- checkbox -->
                        <div class="form-group">
                            <label>
                                <input type="checkbox" class="minimal" name="is_coming_soon">
                            </label>
                            <label class="sidebar-checkbox-label">
                                В разработке
                            </label>
                        </div>

                        <!-- checkbox -->
                        <div class="form-group">
                            <label>
                                <input type="checkbox" class="minimal" name="is_features">
                            </label>
                            <label class="sidebar-checkbox-label">
                                Рекомендовать
                            </label>
                        </div>

                        <!-- checkbox -->
                        <div class="form-group">
                            <label>
                                <input type="checkbox" class="minimal" name="status">
                            </label>
                            <label class="sidebar-checkbox-label">
                                Черновик
                            </label>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="label-from-group">
                                    <label>Баннер игры<span class="required">*</span></label>
                                    <div class="helpers">
                                        <span class="help">?</span>
                                        <span class="description_help">Загружайте изображение по размерам: 1920x700</span>
                                    </div>
                                </div>
                                <input type="file" name="banner_game" id="banner_game" class="inputfiles" data-multiple-caption="{count} files selected" multiple />
                                <label for="banner_game" class="labelinputfiles">Выбрать баннера</label>
                                <span class="file_names"></span>
                                <p class="help-block">jpg, jpeg, png</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="label-from-group">
                                    <label>Миниатюра игры<span class="required">*</span></label>
                                    <div class="helpers">
                                        <span class="help">?</span>
                                        <span class="description_help">Загружайте изображение по размерам: 512x512</span>
                                    </div>
                                </div>
                                <input type="file" name="thumbnail_game" id="thumbnail_game" class="inputfiles" data-multiple-caption="{count} files selected" multiple />
                                <label for="thumbnail_game" class="labelinputfiles">Выбрать иконку</label>
                                <span class="file_names"></span>
                                <p class="help-block">jpg, jpeg, png</p>
                            </div>
                        </div>

                        <!-- Gallery game -->
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="label-from-group">
                                    <label>Изображения/скриншоты</label>
                                    <div class="helpers">
                                        <span class="help">?</span>
                                        <span class="description_help">Загружайте изображение по размерам: 700x600</span>
                                    </div>
                                </div>
                                <input type="file" name="files_gallery[]" id="files_gallery" class="inputfiles" data-multiple-caption="{count} files selected" multiple />
                                <label for="files_gallery" class="labelinputfiles">Выбрать файлы</label>
                                <span class="file_names"></span>
                                <p class="help-block">jpg, jpeg, png</p>
                            </div>
                        </div>

                    </div>
                </div>

                <!-- /.box-body -->
                <div class="box-footer">
                    <button class="btn btn-default">Назад</button>
                    <button class="btn btn-success pull-right">Добавить</button>
                </div>
                <!-- /.box-footer-->
            </div>
            <!-- /.box -->
            {{Form::close()}}
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
