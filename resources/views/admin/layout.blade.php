<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>HiMeli | Admin Panel</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <script src="https://cdn.tiny.cloud/1/cc96leyouwqirwu4jpi3qcp3iigcnpx8zye70b69oobfnhg0/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

    <link rel="stylesheet" href="/css/admin.css">
    <style>
        table.table form{
            display: inline-block;
        }
        button.delete{
            background: transparent;
            border: none;
            color: #337ab7;
            padding: 0px;
        }
    </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

    <header class="main-header">
        <!-- Logo -->
        <a href="/" class="logo">
            <!-- mini logo for sidebar mini 50x50 pixels -->
            <span class="logo-mini"><b>Hi</b></span>
            <!-- logo for regular state and mobile devices -->
            <span class="logo-lg"><b>Admin</b>Panel</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top">
            <!-- Sidebar toggle button-->
            <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>

            <div class="navbar-custom-menu">
                <ul class="nav navbar-nav">
{{--                    <!-- Messages: style can be found in dropdown.less-->--}}
{{--                    <li class="dropdown messages-menu">--}}
{{--                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">--}}
{{--                            <i class="fa fa-envelope-o"></i>--}}
{{--                            <span class="label label-success">4</span>--}}
{{--                        </a>--}}
{{--                        <ul class="dropdown-menu">--}}
{{--                            <li class="header">You have 4 messages</li>--}}
{{--                            <li>--}}
{{--                                <!-- inner menu: contains the actual data -->--}}
{{--                                <ul class="menu">--}}
{{--                                    <li><!-- start message -->--}}
{{--                                        <a href="#">--}}
{{--                                            <div class="pull-left">--}}
{{--                                                <img src="/public/img/user2-160x160.jpg" class="img-circle" alt="User Image">--}}
{{--                                            </div>--}}
{{--                                            <h4>--}}
{{--                                                Support Team--}}
{{--                                                <small><i class="fa fa-clock-o"></i> 5 mins</small>--}}
{{--                                            </h4>--}}
{{--                                            <p>Why not buy a new awesome theme?</p>--}}
{{--                                        </a>--}}
{{--                                    </li>--}}
{{--                                    <!-- end message -->--}}
{{--                                </ul>--}}
{{--                            </li>--}}
{{--                            <li class="footer"><a href="#">See All Messages</a></li>--}}
{{--                        </ul>--}}
{{--                    </li>--}}
{{--                    <!-- Notifications: style can be found in dropdown.less -->--}}
{{--                    <li class="dropdown notifications-menu">--}}
{{--                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">--}}
{{--                            <i class="fa fa-bell-o"></i>--}}
{{--                            <span class="label label-warning">10</span>--}}
{{--                        </a>--}}
{{--                        <ul class="dropdown-menu">--}}
{{--                            <li class="header">You have 10 notifications</li>--}}
{{--                            <li>--}}
{{--                                <!-- inner menu: contains the actual data -->--}}
{{--                                <ul class="menu">--}}
{{--                                    <li>--}}
{{--                                        <a href="#">--}}
{{--                                            <i class="fa fa-users text-aqua"></i> 5 new members joined today--}}
{{--                                        </a>--}}
{{--                                    </li>--}}
{{--                                </ul>--}}
{{--                            </li>--}}
{{--                            <li class="footer"><a href="#">View all</a></li>--}}
{{--                        </ul>--}}
{{--                    </li>--}}
{{--                    <!-- Tasks: style can be found in dropdown.less -->--}}
{{--                    <li class="dropdown tasks-menu">--}}
{{--                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">--}}
{{--                            <i class="fa fa-flag-o"></i>--}}
{{--                            <span class="label label-danger">9</span>--}}
{{--                        </a>--}}
{{--                        <ul class="dropdown-menu">--}}
{{--                            <li class="header">You have 9 tasks</li>--}}
{{--                            <li>--}}
{{--                                <!-- inner menu: contains the actual data -->--}}
{{--                                <ul class="menu">--}}
{{--                                    <li><!-- Task item -->--}}
{{--                                        <a href="#">--}}
{{--                                            <h3>--}}
{{--                                                Design some buttons--}}
{{--                                                <small class="pull-right">20%</small>--}}
{{--                                            </h3>--}}
{{--                                            <div class="progress xs">--}}
{{--                                                <div class="progress-bar progress-bar-aqua" style="width: 20%" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">--}}
{{--                                                    <span class="sr-only">20% Complete</span>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </a>--}}
{{--                                    </li>--}}
{{--                                    <!-- end task item -->--}}
{{--                                </ul>--}}
{{--                            </li>--}}
{{--                            <li class="footer">--}}
{{--                                <a href="#">View all tasks</a>--}}
{{--                            </li>--}}
{{--                        </ul>--}}
{{--                    </li>--}}
{{--                    <!-- User Account: style can be found in dropdown.less -->--}}
                    <li class="dropdown user user-menu">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <img src="{{Auth::user()->getAvatar()}}" class="user-image" alt="User Image">
                            <span class="hidden-xs">{{Auth::user()->name}}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- User image -->
                            <li class="user-header">
                                <img src="{{Auth::user()->getAvatar()}}" class="img-circle" alt="User Image">
                                <p>{{Auth::user()->name}} - HiMeli(Admin)</p>
                            </li>
{{--                            <!-- Menu Body -->--}}
{{--                            <li class="user-body">--}}
{{--                                <div class="row">--}}
{{--                                    <div class="col-xs-4 text-center">--}}
{{--                                        <a href="#">Followers</a>--}}
{{--                                    </div>--}}
{{--                                    <div class="col-xs-4 text-center">--}}
{{--                                        <a href="#">Sales</a>--}}
{{--                                    </div>--}}
{{--                                    <div class="col-xs-4 text-center">--}}
{{--                                        <a href="#">Friends</a>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                                <!-- /.row -->--}}
{{--                            </li>--}}
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="/profile/" class="btn btn-default btn-flat">Profile</a>
                                </div>
                                <div class="pull-right">
                                    <a href="/logout/" class="btn btn-default btn-flat">Sign out</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <!-- Control Sidebar Toggle Button -->
{{--                    <li>--}}
{{--                        <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>--}}
{{--                    </li>--}}
                </ul>
            </div>
        </nav>
    </header>

    @include('admin._sidebar')

    <!-- Content Wrapper. Contains page content -->

    @yield('content')
    <!-- /.content-wrapper -->

    <footer class="main-footer">
        <div class="pull-right hidden-xs">
            <b>Version</b> 2.21.10.1
        </div>
        <strong>Copyright &copy; 2021 <a href="https://himeli.org/">HiMeli</a>.</strong> All rights reserved.
    </footer>

    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<script src="/js/admin.js"></script>

</body>
</html>
