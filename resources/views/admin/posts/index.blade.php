@extends('admin.layout')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Посты</h1>
            <ol class="breadcrumb">
                <li><a href="/admin/"><i class="fa fa-dashboard"></i> Админ-панель</a></li>
                <li class="active">Посты</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Default box -->
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title"></h3>
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{session('status')}}
                        </div>
                    @endif
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="form-group">
                        <a href="{{route('posts.create')}}" class="btn btn-success">Добавить пост</a>
                    </div>
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Название</th>
                            <th>Категория</th>
                            <th>Теги</th>
                            <th>Картинка</th>
                            <th>Действия</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($posts as $post)
                                <tr>
                                    <td>{{$post->id}}</td>
                                    <td><a href="/news/{{$post->slug}}" target="_blank">{{$post->title}}</a>@if($post->status != 0) {{" - Черновик"}}@endif</td>
                                    <td>{{$post->getCategoryTitle()}}</td>
                                    <td>{{$post->getTagsTitles()}}</td>
                                    <td>
                                        <img src="{{$post->getImage()}}" alt="" width="100">
                                    </td>
                                    <td>
                                        <a href="{{route('posts.edit', $post->id)}}" class="fa fa-pencil"></a>

                                        {{Form::open( ['route' => ['posts.destroy', $post->id], 'method'=>'delete'] )}}
                                            <button onclick="return confirm('Вы уверены?')" type="submit" class="delete"><i class="fa fa-remove"></i></button>
                                        {{Form::close()}}

                                        @if($post->status === 0)
                                            <a href="{{route('telegramAPI.send_request', $post->id)}}"
                                               onclick="return confirm('Вы действительно хотите отправить новость в телеграм?')"
                                               class="fa fa-rss">
                                            </a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
