@extends('admin.layout')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Изменить статью <strong>{{ $post->title }}</strong></h1>
        </section>

        <!-- Main content -->
        <section class="content">
            {{ Form::open([
                'route' => ['posts.update', $post->id],
                'files' => true,
                'method'=> 'put'
            ]) }}
            <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    @include('admin.errors')
                </div>
                <div class="box-body">
                    <div class="col-md-9">
                        <div class="form-group">
                            <label for="slug">Ссылка</label>
                            <a href="https://himeli.org/news/{{ $post->slug }}/" target="_blank">https://himeli.org/news/{{ $post->slug }}/</a>
                            <input type="text" class="form-control" id="slug" name="slug" value="{{ $post->slug }}">
                        </div>
                        <div class="form-group">
                            <label for="title">Название<span class="required">*</span></label>
                            <input type="text" class="form-control" id="title" name="title" value="{{ $post->title }}">
                        </div>
                        <div class="form-group">
                            <label for="keywords">Ключевые слова<span class="required">*</span></label>
                            <input type="text" class="form-control" id="keywords" name="keywords" value="{{ $post->keywords }}">
                        </div>
                        <div class="form-group">
                            <label for="excerpt">Короткий текст<span class="required">*</span></label>
                            <textarea name="excerpt" id="excerpt" cols="30" rows="5" class="form-control">{{ $post->the_excerpt() }}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="content">Полный текст<span class="required">*</span></label>
                            <textarea name="content" id="content" cols="50" rows="30" class="form-control">{{ $post->the_content() }}</textarea>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Дата релиза/последнего обновления<span class="required">*</span></label>
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right" id="datepicker" name="date" value="{{ $post->date }}" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group">
                            <label>
                                {{ Form::checkbox('is_featured', '1', $post->is_featured, ['class' => 'minimal']) }}
                            </label>
                            <label>Рекомендовать</label>
                        </div>
                        <!-- checkbox -->
                        <div class="form-group">
                            <label>
                                {{ Form::checkbox('status', '1', $post->status, ['class' => 'minimal']) }}
                            </label>
                            <label>Черновик</label>
                        </div>
                        <div class="form-group">
                            <label for="views">Кол-во просмотров<span class="required">*</span></label>
                            <input type="text" class="form-control" id="views" name="views" value="{{ $post->views }}">
                        </div>
                        <div class="form-group">
                            <label for="time">Минут на чтение<span class="required">*</span></label>
                            <input type="number" class="form-control" id="time" name="time" value="{{ $post->time }}">
                        </div>
                        <div class="form-group">
							<?php $empty = ['Без категории']; ?>
                            <label>Категория</label>
                            {{
                                Form::select('category_id',
                                $empty + $categories,
                                $post->getCategoryID(),
                                ['class' => 'form-control select2'])
                            }}
                        </div>
                        <div class="form-group">
                            <label>Теги</label>
                            {{ Form::select('tags[]',
                                $tags,
                                $selectedTags,
                                ['class' => 'form-control select2', 'multiple'=>'multiple','data-placeholder'=>'Выберите теги'])
                            }}
                        </div>
                        <div class="form-group">
                            <div class="label-from-group">
                                <label for="image">Лицевая картинка<span class="required">*</span></label>
                                <div class="helpers">
                                    <span class="help">?</span>
                                    <span class="description_help">Загружайте изображение по размерам: 840x580</span>
                                </div>
                            </div>
                            <img src="{{ $post->getImage() }}" class="img-responsive" width="200" style="margin: 10px 0;">
                            <input type="file" id="image" name="image">
                            <p class="help-block">jpg, jpeg, png</p>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button class="btn btn-default">Назад</button>
                    <button class="btn btn-warning pull-right">Изменить</button>
                </div>
                <!-- /.box-footer-->
            </div>
            <!-- /.box -->
            {{ Form::close() }}
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
