@extends('admin.layout')

@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper create-post">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>Добавить статью</h1>
        </section>

        <!-- Main content -->
        <section class="content">
            {{ Form::open([
                'route' => 'posts.store',
                'files' => true
            ]) }}
            <!-- Default box -->

            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title"></h3>
                    @include('admin.errors')
                </div>
                <div class="box-body">
                    <div class="col-md-9">
                        <div class="form-group">
                            <label for="title">Название<span class="required">*</span></label>
                            <input type="text" class="form-control" id="title" placeholder="" name="title" value="{{ old('title') }}">
                        </div>
                        <div class="form-group">
                            <label for="keywords">Ключевые слова<span class="required">*</span></label>
                            <input type="text" class="form-control" id="keywords" placeholder="" name="keywords" value="{{ old('keywords') }}">
                        </div>
                        <div class="form-group">
                            <label for="excerpt">Короткий текст<span class="required">*</span></label>
                            <textarea name="excerpt" id="excerpt" cols="30" rows="5" class="form-control"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="content">Полный текст<span class="required">*</span></label>
                            <textarea name="content" id="content" cols="50" rows="30" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Дата релиза/последнего обновления<span class="required">*</span></label>
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right" id="datepicker" name="date" value="{{ old('date') }}" autocomplete="off">
                            </div>
                        </div>
                        <div class="form-group">
                            <label>
                                <input type="checkbox" class="minimal" name="is_features">
                            </label>
                            <label>Рекомендовать</label>
                        </div>
                        <div class="form-group">
                            <label>
                                <input type="checkbox" class="minimal" name="status">
                            </label>
                            <label>Черновик</label>
                        </div>
                        <div class="form-group">
                            <label for="views">Кол-во просмотров<span class="required">*</span></label>
                            <input type="text" class="form-control" id="views" name="views" value="0">
                        </div>
                        <div class="form-group">
                            <label for="time">Минут на чтение<span class="required">*</span></label>
                            <input type="number" class="form-control" id="time" name="time" value="4">
                        </div>
                        <div class="form-group">
                            <label>Категория</label>
                            <?php $empty = ['Без категории']; ?>
                            {{ Form::select('category_id',
                                $empty + $categories,
                                null,
                                ['class' => 'form-control select2', 'data-placeholder'=>'Выберите категорию'])
                            }}
                        </div>
                        <div class="form-group">
                            <label>Теги</label>
                            {{ Form::select('tags[]',
                                $tags,
                                null,
                                ['class' => 'form-control select2', 'multiple' => 'multiple', 'data-placeholder' => 'Выберите теги'])
                            }}
                        </div>
                        <div class="form-group">
                            <div class="label-from-group">
                                <label for="image">Лицевая картинка<span class="required">*</span></label>
                                <div class="helpers">
                                    <span class="help">?</span>
                                    <span class="description_help">Загружайте изображение по размерам: 840x580</span>
                                </div>
                            </div>
                            <input type="file" id="image" name="image">
                            <p class="help-block">Формат: .jpg, .png, .jpeg</p>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button class="btn btn-default">Назад</button>
                    <button class="btn btn-success pull-right">Добавить</button>
                </div>
                <!-- /.box-footer-->
            </div>
            <!-- /.box -->
            {{ Form::close() }}
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection
