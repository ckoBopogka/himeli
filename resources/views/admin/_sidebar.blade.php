<!-- =============================================== -->

<!-- Left side column. contains the sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
{{--        <div class="user-panel">--}}
{{--            <div class="pull-left image">--}}
{{--                <img src="/img/user2-160x160.jpg" class="img-circle" alt="User Image">--}}
{{--            </div>--}}
{{--            <div class="pull-left info">--}}
{{--                <p>Denys Polkovnyk</p>--}}
{{--            </div>--}}
{{--        </div>--}}
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
                <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">Навигация</li>
            <li class="treeview">
                <a href="/admin/">
                    <i class="fa fa-dashboard"></i> <span>Админ панель</span>
                </a>
            </li>
            <li>
                <a href="{{route('pages.index')}}">
                    <i class="fa fa-book"></i>
                    <span>Страницы</span>
                </a>
            </li>
            <li>
                <a href="{{route('posts.index')}}">
                    <i class="fa fa-sticky-note-o"></i>
                    <span>Посты</span>
                </a>
            </li>
            <li>
                <a href="{{route('categories.index')}}">
                    <i class="fa fa-list-ul"></i>
                    <span>Категории</span>
                </a>
            </li>
            <li>
                <a href="{{route('tags.index')}}">
                    <i class="fa fa-tags"></i>
                    <span>Теги</span>
                </a>
            </li>
            <li>
                <a href="{{route('posts-game.index')}}">
                    <i class="fa fa-futbol-o"></i>
                    <span>Игры</span>
                </a>
            </li>
            <li>
                <a href="{{route('users.index')}}">
                    <i class="fa fa-users"></i>
                    <span>Пользователи</span>
                </a>
            </li>
            <li>
                <a href="{{route('subscribers.index')}}">
                    <i class="fa fa-user-plus"></i>
                    <span>Подписчики</span>
                </a>
            </li>
            <li>
                <a href="{{route('vacancies.index')}}">
                    <i class="fa ion-social-linkedin"></i>
                    <span>Вакансии</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-pie-chart"></i>
                    <span>Статистика</span>
                </a>
            </li>
            <li>
                <a href="#">
                    <i class="fa fa-street-view"></i>
                    <span>Письма</span>
                </a>
            </li>
            <li>
                <a href="{{route('settings.index')}}">
                    <i class="fa fa fa-sliders"></i>
                    <span>Настройки</span>
                </a>
            </li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>

<!-- =============================================== -->
